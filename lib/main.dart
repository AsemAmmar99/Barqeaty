import 'package:barkeaty/shared/network/local/helpers/cache_helper.dart';
import 'package:barkeaty/shared/network/remote/service/my_service.dart';
import 'package:barkeaty/shared/network/remote/helpers/dio_helper.dart';
import 'package:barkeaty/shared/models/service_model.dart';
import 'package:barkeaty/stateful/sign_in.dart';
import 'package:barkeaty/stateful/splash_screen.dart';
import 'package:barkeaty/stateless/about_app.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'stateless/whoAreWe.dart';
import 'package:barkeaty/stateful/home.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();

  DioHelper.init();
  await CacheHelper.init();

  Widget widget;

  widget = const MySplashScreen();

  runApp(MyApp(startWidget: widget,));
}

class MyApp extends StatelessWidget {

  final Widget startWidget;

  const MyApp({Key? key, required this.startWidget}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
    ChangeNotifierProvider<MyService>(create: (context) => MyService(),
    ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: startWidget,

      ),
    );
  }
}


