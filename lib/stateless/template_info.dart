import 'package:barkeaty/shared/network/local/helpers/cache_helper.dart';
import 'package:barkeaty/shared/network/remote/service/my_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

class TemplateInfo extends StatelessWidget {
  const TemplateInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
        return Scaffold(
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            child: SizedBox(
              height: 100.h,
              child: Stack(
                children: [
                  Container(
                    color: const Color.fromRGBO(1, 18, 112, 1),
                    height: 35.h,
                  ),
                  Positioned(
                    top: 2.h,
                    left: 30.w,
                    child: Image(
                        width: 50.w,
                        height: 25.h,
                        image: AssetImage("android/assets/barqiaty.png")),
                  ),
                  Positioned(
                    top: 10.h,
                    left: 6.w,
                    child: GestureDetector(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Image(
                          width: 15.w,
                          height: 15.w,
                          image: AssetImage("android/assets/down-arrow-1.png")),
                    ),
                  ),
                  Positioned(
                    top: 25.h,
                    left: 3.5.w,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      width: 92.w,
                      height: 57.h,
                    ),
                  ),
                  Positioned(
                    top: 26.h,
                    left: 22.w,
                    child: Text("نموذج ${Provider.of<MyService>(context,listen: false).getTemplatesResp!.templates[CacheHelper.getData(key: 'tempInd')].name}",
                      style: TextStyle(
                      fontSize: 5.w,
                      //color: Colors.white,
                      fontFamily: "theFont",
                      color: const Color.fromRGBO(1, 18, 112, 1),
                    ),
                    ),
                  ),
                  Positioned(
                    top: 34.h,
                    left: 15.w,
                    right: 15.w,
                    child: Container(
                      height: 0.2.w,
                      width: 100.w,
                    color: Colors.black,
                  ),
                  ),
                  Positioned(
                    top: 34.h,
                    right: 15.w,
                    child: Container(
                      alignment: Alignment.centerRight,
                      height: 30.h,
                      width: 70.w,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                              Provider.of<MyService>(context,listen: false).getTemplatesResp!.templates[CacheHelper.getData(key: 'tempInd')].text,
                            textAlign: TextAlign.right,
                            style: TextStyle(
                              fontSize: 3.w,
                              //color: Colors.white,
                              fontFamily: "theFont",
                              color: const Color.fromRGBO(1, 18, 112, 1),
                            ),
                          ),
                        ],
                      ),
                    ),
                    ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
