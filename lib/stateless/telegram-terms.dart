// ignore_for_file: file_names, use_key_in_widget_constructors
import 'package:barkeaty/shared/network/remote/service/my_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:url_launcher/url_launcher.dart';

class telegram_terms extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
        return Scaffold(
          backgroundColor: Colors.white,
          body: SizedBox(
            height: 160.h,
            child: Stack(
              children: [
                Container(
                  color: const Color.fromRGBO(1, 18, 112, 1),
                  height: 35.h,
                ),
                Positioned(
                  top: 2.h,
                  left: 30.w,
                  child: Image(
                      width: 50.w,
                      height: 25.h,
                      image: AssetImage("android/assets/barqiaty.png")),
                ),
                Positioned(
                  top: 10.h,
                  left: 6.w,
                  child: GestureDetector(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Image(
                        width: 15.w,
                        height: 15.w,
                        image: AssetImage("android/assets/down-arrow-1.png")),
                  ),
                ),
                Positioned(
                  top: 20.h,
                  left: 3.5.w,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    width: 92.w,
                    height: 77.h,
                  ),
                ),
                Positioned(
                  top: 22.h,
                  left: 18.w,
                  child: Text("الشروط والاحكام لارسال البرقية",style: TextStyle(
                    fontSize: 5.w,
                    //color: Colors.white,
                    fontFamily: "theFont",
                    color: const Color.fromRGBO(1, 18, 112, 1),
                  ),),
                ),
                Positioned(
                  top: 26.h,
                  right: 5.w,
                  child: Container(
                    height: 67.h,
                    width: 90.w,
                    alignment: AlignmentDirectional.center,
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Html(
                              data: Provider.of<MyService>(context,listen: false).getLetterTermsResp!.termsData.text,
                              onLinkTap: (String? url, RenderContext context, Map<String, String> attributes, element) {
                                launch(url!);
                              }
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
