import 'package:barkeaty/shared/network/local/helpers/cache_helper.dart';
import 'package:barkeaty/shared/network/remote/service/my_service.dart';
import 'package:barkeaty/stateful/home.dart';
import 'package:barkeaty/stateful/personal_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';


class destination extends StatefulWidget {
  @override
  _destinationState createState() => _destinationState();
}

class _destinationState extends State<destination> {

  dynamic dropdownValue1;
  List _typesOfBreed = [];
  var formKey = GlobalKey<FormState>();
  bool loading = false;

  @override
  Widget build(BuildContext context) {

    if(CacheHelper.getData(key: 'serviceid') == 5){
      _typesOfBreed = Provider.of<MyService>(context,listen: false).getDirectionsResp!.directions;// here you can declare your list instance.
    }else{
      _typesOfBreed = Provider.of<MyService>(context,listen: false).getPrivateCompaniesResp!.privateCompanies;
    }

    return Sizer(
      builder: (context, orientation, deviceType) {
        return Scaffold(
          backgroundColor: Colors.white,
          body: Form(
            key: formKey,
            child: SingleChildScrollView(
              child: SizedBox(
                height: 100.h,
                child: Stack(
                  children: [
                    Container(
                      color: const Color.fromRGBO(1, 18, 112, 1),
                      height: 35.h,
                    ),
                    Positioned(
                      top: 2.h,
                      left: 30.w,
                      child: Image(
                          width: 50.w,
                          height: 25.h,
                          image: AssetImage("android/assets/barqiaty.png")),
                    ),
                    Positioned(
                      top: 10.h,
                      left: 6.w,
                      child: GestureDetector(
                        onTap: () async{
                          await Provider.of<MyService>(context,listen: false).unseenNotifications();
                          Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Home(),
                            ),
                                (route) {
                              return false;
                            },
                          );
                        },
                        child: Image(
                            width: 15.w,
                            height: 15.w,
                            image: AssetImage("android/assets/down-arrow-1.png")),
                      ),
                    ),
                    Positioned(
                      top: 25.h,
                      left: 3.5.w,
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: Offset(0, 3), // changes position of shadow
                            ),
                          ],
                        ),
                        width: 92.w,
                        height: 65.h,
                      ),
                    ),
                    Positioned(
                      top: 26.h,
                      left: 22.w,
                      child: Text("الجهة المراد الارسال اليها",style: TextStyle(
                        fontSize: 5.5.w,
                        //color: Colors.white,
                        fontFamily: "theFont",
                        color: const Color.fromRGBO(1, 18, 112, 1),
                      ),),
                    ),
                    Positioned(
                      top: 75.h,
                      left: 12.w,
                      child: GestureDetector(
                        onTap: (){
                          if(formKey.currentState!.validate()) {
                            Navigator.push(context,
                              MaterialPageRoute(
                                builder: (context) => personal_image(),
                              ),);
                          }
                        },
                        child: Container(
                          width: 35.w,
                          height: 7.h,
                          alignment: Alignment.center,
                          decoration: const BoxDecoration(
                            color: Color.fromRGBO(105, 203, 248, 1),
                            borderRadius: BorderRadius.all(Radius.circular(50.0)),
                          ),
                          child: Text("التالي",style: TextStyle(
                            fontSize: 6.5.w,
                            color: Colors.white,
                            fontFamily: "theFont",
                          ),),
                        ),
                      ),
                    ),
                    Positioned(
                      top: 75.h,
                      left: 51.w,
                      child: GestureDetector(
                        onTap: (){
                          Navigator.pop(context);
                        },
                        child: Container(
                          width: 35.w,
                          height: 7.h,
                          alignment: Alignment.center,
                          decoration: const BoxDecoration(
                            color: Color.fromRGBO(105, 203, 248, 1),
                            borderRadius: BorderRadius.all(Radius.circular(50.0)),
                          ),
                          child: Text("السابق",style: TextStyle(
                            fontSize: 6.5.w,
                            color: Colors.white,
                            fontFamily: "theFont",
                          ),),
                        ),
                      ),
                    ),
                    // Positioned(
                    //   top: 45.h,
                    //   left: 23.w,
                    //   child: Container(
                    //   width: 25.h,
                    //   height: 25.h,
                    //     child: Image(
                    //       width: 25.h,
                    //       height: 25.h,
                    //       image: const AssetImage("android/assets/box-rita-img-02-133x125.png"),
                    //     ),
                    // )),
                    Positioned(
                      top: 35.h,
                      left: 17.w,
                      child: SizedBox(
                        width: 65.w,
                        height: 10.h,
                        child: DropdownButtonHideUnderline(
                          child: DropdownButtonFormField<String>(
                            isExpanded: true,
                            icon: Image(image: const AssetImage("android/assets/down-arrow.png"),
                              width: 5.w,height: 5.h,),
                            //borderRadius: BorderRadius.circular(5.w),
                            style: const TextStyle(color: const Color.fromRGBO(112, 116, 162, 1),
                                fontFamily: "theFont"),
                            onChanged: (String? newValue) {
                              setState((){
                                dropdownValue1 = newValue!;
                                if(CacheHelper.getData(key: 'serviceid') == 5) {
                                  if(CacheHelper.getData(key: 'privateCompanyId') != null) {
                                    CacheHelper.removeData(key: 'privateCompanyId');
                                  }
                                  CacheHelper.saveData(key: 'directionId', value: dropdownValue1);
                                }else if(CacheHelper.getData(key: 'serviceid') == 6)
                                  {
                                    if(CacheHelper.getData(key: 'directionId') != null) {
                                      CacheHelper.removeData(key: 'directionId');
                                    }
                                    CacheHelper.saveData(key: 'privateCompanyId', value: dropdownValue1);
                                  }
                              });
                            },
                            value: dropdownValue1,
                            hint: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                const Text(
                                  '..اختر الجهة',
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.center,
                                ),
                                SizedBox(width: 2.w,),
                              ],
                            ),
                            validator: (value) {
                              if (value == null) {
                                return 'اختر الجهة..';
                              }
                            },
                            items: _typesOfBreed
                                .map((value) {
                              return DropdownMenuItem(
                                value: value.id.toString(),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Text(
                                      value.name,
                                      textAlign: TextAlign.center,
                                    ),
                                    SizedBox(width: 2.w,),
                                  ],
                                ),
                              );
                            }).toList(),
                          ),
                        ),
                        /*TextFormField(
                          onChanged: (val){},
                          enabled: true,
                          cursorColor: Colors.black26,
                            contentPadding: EdgeInsets.all(1.w),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(50.0),
                            ),
                          ),
                        ),*/
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
