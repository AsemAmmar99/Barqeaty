import 'package:barkeaty/shared/models/private_company_model.dart';
import 'package:barkeaty/shared/network/remote/service/my_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';



class personal_companies_list extends StatefulWidget {
  @override
  State<personal_companies_list> createState() => _personal_companies_listState();
}

class _personal_companies_listState extends State<personal_companies_list> {

  var searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
        return Scaffold(
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            child: SizedBox(
              height: 100.h,
              child: Column(
                children: [
                  Stack(
                    children: [
                      Container(
                        color: const Color.fromRGBO(1, 18, 112, 1),
                        height: 35.h,
                      ),
                      Positioned(
                        top: 2.h,
                        left: 30.w,
                        child: Image(
                            width: 50.w,
                            height: 25.h,
                            image: AssetImage("android/assets/barqiaty.png")),
                      ),
                      Positioned(
                        top: 20.h,
                        left: 6.w,
                        child: GestureDetector(
                          onTap: (){
                            Navigator.pop(context);
                          },
                          child: Image(
                              width: 15.w,
                              height: 15.w,
                              image: AssetImage("android/assets/straight-right-arrow.png")),
                        ),
                      ),
                      Positioned(
                        top: 21.h,
                        left: 28.w,
                        child:  Container(
                          width: 57.w,
                          height: 4.5.h,
                          decoration: const BoxDecoration(
                            //color: Color.fromRGBO(245, 153, 45, 1),
                            borderRadius: BorderRadius.all(Radius.circular(50.0)),
                          ),
                          child: TextFormField(
                            controller: searchController,
                            onChanged: onSearchTextChanged,
                            textAlign: TextAlign.center,
                            enabled: true,
                            style: TextStyle(
                              fontFamily: 'theFont',
                              fontSize: 4.w,
                              color: Colors.white,
                            ),
                            cursorColor: const Color.fromRGBO(3, 141, 151, 1),
                            decoration: InputDecoration(
                              prefixIcon: Padding(
                                padding: EdgeInsets.zero, // add padding to adjust icon
                                child: Icon(Icons.search,
                                  color: Colors.white,
                                  size: 7.w,),
                              ),
                              contentPadding: EdgeInsets.all(1.w),
                              isDense: true,
                              fillColor: const Color.fromRGBO(112, 116, 162, 1),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(50.0),
                              ),
                              filled: true,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      SizedBox(width: 25.h,),
                      Text("قائمة الشركات الخاصة",style: TextStyle(
                        fontSize: 5.w,
                        fontFamily: "theFont",
                        color: const Color.fromRGBO(1, 18, 112, 1),
                      ),),
                    ],
                  ),
                  Expanded(
                    child: _searchResult.length != 0 || searchController.text.isNotEmpty
                        ? ListView.builder(
                    shrinkWrap: true,
                    itemBuilder: (context, i) => Container(
                      margin: const EdgeInsets.only(top: 0, left: 25, right: 25, bottom: 15,),
                      padding: const EdgeInsets.all(20),
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.35),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: Offset(0, 3), // changes position of shadow
                            ),
                          ],
                          borderRadius: BorderRadius.circular(15),
                          color: Colors.white),
                      child: Column(
                        children: [
                          Text(
                            _searchResult[i].name,
                            textAlign: TextAlign.center,
                            style: const TextStyle(
                              color: Color.fromRGBO(
                                  1, 18, 112, 1),
                            ),
                          ),
                        ],
                      ),
                    ),
                    itemCount: _searchResult.length,
                  ) : ListView.builder(
                      shrinkWrap: true,
                      itemBuilder: (context, index) => Container(
                        margin: const EdgeInsets.only(top: 0, left: 25, right: 25, bottom: 15,),
                        padding: const EdgeInsets.all(20),
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.35),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset: Offset(0, 3), // changes position of shadow
                              ),
                            ],
                            borderRadius: BorderRadius.circular(15),
                            color: Colors.white),
                        child: Column(
                          children: [
                            Text(
                              Provider.of<MyService>(context,listen: false).getPrivateCompaniesResp!.privateCompanies[index].name,
                              textAlign: TextAlign.center,
                              style: const TextStyle(
                                color: Color.fromRGBO(
                                    1, 18, 112, 1),
                              ),
                            ),
                          ],
                        ),
                      ),
                      itemCount: Provider.of<MyService>(context,listen: false).getPrivateCompaniesResp!.privateCompanies.length,
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    Provider.of<MyService>(context,listen: false).getPrivateCompaniesResp!.privateCompanies.forEach((privateCompany) {
      if (privateCompany.name.contains(text)) {
        _searchResult.add(privateCompany);
      }
    });

    setState(() {});
  }
}

List<PrivateCompany> _searchResult = [];