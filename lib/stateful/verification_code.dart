//this file is not used in project

import 'package:barkeaty/shared/network/remote/service/my_service.dart';
import 'package:barkeaty/stateful/personal_data.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

import 'home.dart';


class code extends StatefulWidget {
  @override
  _codeState createState() => _codeState();
}

class _codeState extends State<code> {
  var firstNoController = TextEditingController();
  var secondNoController = TextEditingController();
  var thirdNoController = TextEditingController();
  var forthNoController = TextEditingController();
  FocusNode f1 = FocusNode();
  FocusNode f2 = FocusNode();
  FocusNode f3 = FocusNode();
  FocusNode f4 = FocusNode();
  bool loading = false;
  bool resendLoading = false;

  var formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
        return Scaffold(
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  Row(
                    children: [
                      SizedBox(width: 30.w,),
                      Image(
                          width: 50.w,
                          height: 25.h,
                          image: AssetImage("android/assets/barqiaty.png")),
                    ],
                  ),
                  Row(
                    children: [
                      SizedBox(width: 54.w,),
                      Text("رمز التحقق",
                        style: TextStyle(
                          color: const Color.fromRGBO(1, 18, 112, 1),
                          fontFamily: "theFont",
                          fontSize: 9.w,
                        ),),
                    ],
                  ),
                  Row(
                    children: [
                      SizedBox(width: 6.w,),
                      Container(
                        width: 90.w,
                        child: Text(
                          "التحقق يتم من خلال منصة الموثق الإلكتروني وسيصلك رقم التحقق على جوالك المسجل في حسابك بالتطبيق",
                          textAlign: TextAlign.end,
                          style: TextStyle(
                            color:  const Color.fromRGBO(112, 116, 162, 1),
                            fontFamily: "theFont",
                            fontSize: 4.w,
                          ),),
                      ),
                    ],
                  ),
                  SizedBox(height: 8.h,),
                  Row(
                    children: [
                      SizedBox(width: 70.w,),
                      Text("رمز التحقق",
                        style: TextStyle(
                          color:  const Color.fromRGBO(112, 116, 162, 1),
                          fontFamily: "theFont",
                          fontSize: 4.8.w,
                        ),),
                    ],
                  ),
                  SizedBox(height: 2.h,),
                  Row(
                    children: [
                      SizedBox(width: 15.w,),
                      Container(
                        width: 15.w,
                        decoration: const BoxDecoration(
                          //color: Color.fromRGBO(245, 153, 45, 1),
                          borderRadius: BorderRadius.all(Radius.circular(50.0)),
                        ),
                        child: TextFormField(
                          textInputAction: TextInputAction.go,
                          keyboardType: TextInputType.number,
                          controller: firstNoController,
                          validator: (value){
                            if (value == null || value.isEmpty) {
                              return '.أدخل رقم التفعيل كاملا';
                            }
                          },
                          focusNode: f1,
                          onChanged: (str) {
                            if (str.length == 1) {
                              FocusScope.of(context).requestFocus(f2);
                            }
                          },
                          onFieldSubmitted: (value){
                            FocusScope.of(context).focusedChild;
                          },
                          maxLength: 1,
                          style: TextStyle(
                            fontFamily: 'theFont',
                            fontSize: 4.5.w,
                            color: Colors.white,
                          ),
                          cursorColor: const Color.fromRGBO(3, 141, 151, 1),
                          decoration: InputDecoration(
                              counterText: "",
                              contentPadding: EdgeInsets.fromLTRB(6.3.w, 2.w, 2.w, 2.w),
                            isDense: true,
                            fillColor: const Color.fromRGBO(1, 18, 112, 1),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(50.0),
                            ),
                            filled: true,
                          ),
                        ),
                      ),
                      SizedBox(width: 5.w,),
                      Container(
                        width: 15.w,
                        decoration: const BoxDecoration(
                          //color: Color.fromRGBO(245, 153, 45, 1),
                          borderRadius: BorderRadius.all(Radius.circular(50.0)),
                        ),
                        child: TextFormField(
                          textInputAction: TextInputAction.go,
                          focusNode: f2,
                          onChanged: (str) {
                            if (str.length == 1) {
                              FocusScope.of(context).requestFocus(f3);
                            }
                          },
                          onFieldSubmitted: (val){
                            FocusScope.of(context).focusedChild;
                          },
                          keyboardType: TextInputType.number,
                          controller: secondNoController,
                          validator: (value){
                            if (value == null || value.isEmpty) {
                              return '.أدخل رقم التفعيل كاملا';
                            }
                          },
                          maxLength: 1,
                          style: TextStyle(
                            fontFamily: 'theFont',
                            fontSize: 4.5.w,
                            color: Colors.white,
                          ),
                          cursorColor: const Color.fromRGBO(3, 141, 151, 1),
                          decoration: InputDecoration(
                            counterText: "",
                            contentPadding: EdgeInsets.fromLTRB(6.3.w, 2.w, 2.w, 2.w),
                            isDense: true,
                            fillColor: const Color.fromRGBO(1, 18, 112, 1),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(50.0),
                            ),
                            filled: true,
                          ),
                        ),
                      ),
                      SizedBox(width: 5.w,),
                      Container(
                        width: 15.w,
                        decoration: const BoxDecoration(
                          //color: Color.fromRGBO(245, 153, 45, 1),
                          borderRadius: BorderRadius.all(Radius.circular(50.0)),
                        ),
                        child: TextFormField(
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.number,
                          controller: thirdNoController,
                          focusNode: f3,
                          onChanged: (str) {
                            if (str.length == 1) {
                              FocusScope.of(context).requestFocus(f4);
                            }
                          },
                          validator: (value){
                            if (value == null || value.isEmpty) {
                              return '.أدخل رقم التفعيل كاملا';
                            }
                          },
                          onFieldSubmitted: (val){
                            FocusScope.of(context).focusedChild;
                          },
                          maxLength: 1,
                          style: TextStyle(
                            fontFamily: 'theFont',
                            fontSize: 4.5.w,
                            color: Colors.white,
                          ),
                          cursorColor: const Color.fromRGBO(3, 141, 151, 1),
                          decoration: InputDecoration(
                            counterText: "",
                            contentPadding: EdgeInsets.fromLTRB(6.3.w, 2.w, 2.w, 2.w),
                            isDense: true,
                            fillColor: const Color.fromRGBO(1, 18, 112, 1),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(50.0),
                            ),
                            filled: true,
                          ),
                        ),
                      ),
                      SizedBox(width: 5.w,),
                      Container(
                        width: 15.w,
                        decoration: const BoxDecoration(
                          //color: Color.fromRGBO(245, 153, 45, 1),
                          borderRadius: BorderRadius.all(Radius.circular(50.0)),
                        ),
                        child: TextFormField(
                          textInputAction: TextInputAction.done,
                          focusNode: f4,
                          onChanged: (str) {
                            if (str.length == 1) {
                              FocusScope.of(context).unfocus();
                            }
                          },
                          onFieldSubmitted: (val){
                              FocusScope.of(context).unfocus();
                            },
                          keyboardType: TextInputType.number,
                          controller: forthNoController,
                          validator: (value){
                            if (value == null || value.isEmpty) {
                              return '.أدخل رقم التفعيل كاملا';
                            }
                          },
                          maxLength: 1,
                          style: TextStyle(
                            fontFamily: 'theFont',
                            fontSize: 4.5.w,
                            color: Colors.white,
                          ),
                          cursorColor: const Color.fromRGBO(3, 141, 151, 1),
                          decoration: InputDecoration(
                            counterText: "",
                            contentPadding: EdgeInsets.fromLTRB(6.3.w, 2.w, 2.w, 2.w),
                            isDense: true,
                            fillColor: const Color.fromRGBO(1, 18, 112, 1),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(50.0),
                            ),
                            filled: true,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 6.h,),
                  resendLoading == false ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextButton(onPressed: () async {
                        setState(() {
                          resendLoading = true;
                        });
                        await  Provider.of<MyService>(context,listen: false).resendCode();
                        if(Provider.of<MyService>(context,listen: false).resendCodeResp!.status == 200){
                          setState(() {
                            resendLoading = false;
                            Fluttertoast.showToast(
                                msg: Provider
                                    .of<MyService>(context, listen: false)
                                    .resendCodeResp!
                                    .message,
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 2,
                                backgroundColor: const Color.fromRGBO(
                                    1, 18, 112, 1),
                                textColor: Colors.white,
                                fontSize: 16.0
                            );
                          });
                        }else{
                          setState(() {
                            resendLoading = false;
                            Fluttertoast.showToast(
                                msg: Provider
                                    .of<MyService>(context, listen: false)
                                    .resendCodeResp!
                                    .message,
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 2,
                                backgroundColor: const Color.fromRGBO(
                                    1, 18, 112, 1),
                                textColor: Colors.white,
                                fontSize: 16.0
                            );
                          });
                        }
                      }, child: Text(
                        '!أرسله مرة أخرى',
                        style: TextStyle(
                          fontFamily: "theFont",
                          fontSize: 4.8.w,
                        ),
                      ),
                      ),
                      SizedBox(width: 2.w,),
                      Text(
                        'لم يصلك رمز التفعيل؟',
                        style: TextStyle(
                          color:  const Color.fromRGBO(112, 116, 162, 1),
                          fontFamily: "theFont",
                          fontSize: 4.8.w,
                        ),
                      ),
                    ],
                  ) : Center(
                    child: Container(
                      width: 50.w,
                      height: 8.h,
                      margin: EdgeInsetsDirectional.only(start: 20.w),
                      alignment: Alignment.center,
                      child: Row(
                        children: const [
                          CircularProgressIndicator.adaptive(
                            backgroundColor: Color.fromRGBO(
                                1, 18, 112, 1),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 10.h,),
                  Row(
                    children: [
                      SizedBox(width: 25.w,),
                      GestureDetector(
                        onTap: () async{
                          if(formKey.currentState!.validate()) {
                            setState(() {
                              loading = true;
                            });
                            await  Provider.of<MyService>(context,listen: false)
                                .validateCode(
                                firstNoController.text+secondNoController.text+thirdNoController.text+forthNoController.text
                            );
                            await Provider.of<MyService>(context,listen: false).unseenNotifications();
                            if (Provider.of<MyService>(context, listen: false).validateCodeResp!.status == 200 && Provider.of<MyService>(context, listen: false).unseenNotificationsResp!.status == 200) {
                              setState(() {
                                loading = false;
                                Fluttertoast.showToast(
                                    msg: Provider
                                        .of<MyService>(context, listen: false)
                                        .validateCodeResp!
                                        .message,
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 2,
                                    backgroundColor: const Color.fromRGBO(
                                        1, 18, 112, 1),
                                    textColor: Colors.white,
                                    fontSize: 16.0
                                );
                              });
                              Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => Home(),
                                ),
                                    (route) {
                                  return false;
                                },
                              );
                            } else {
                              setState(() {
                                loading = false;
                                Fluttertoast.showToast(
                                    msg: Provider
                                        .of<MyService>(context, listen: false)
                                        .validateCodeResp!
                                        .message,
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 2,
                                    backgroundColor: const Color.fromRGBO(
                                        1, 18, 112, 1),
                                    textColor: Colors.white,
                                    fontSize: 16.0
                                );
                              });
                            }
                          }
                        },
                        child: loading == false ? Container(
                          width: 50.w,
                          height: 8.h,
                          alignment: Alignment.center,
                          decoration: const BoxDecoration(
                            color: Color.fromRGBO(105, 203, 248, 1),
                            borderRadius: BorderRadius.all(Radius.circular(50.0)),
                          ),
                          child: Text("تسجيل الدخول",style: TextStyle(
                            fontSize: 6.5.w,
                            color: Colors.white,
                            fontFamily: "theFont",
                          ),),
                        ) : Center(
                          child: Container(
                            width: 50.w,
                            height: 8.h,
                            margin: EdgeInsetsDirectional.only(start: 20.w),
                            alignment: Alignment.center,
                            child: Row(
                              children: const [
                                CircularProgressIndicator.adaptive(
                                  backgroundColor: Color.fromRGBO(
                                      1, 18, 112, 1),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10.h,),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
