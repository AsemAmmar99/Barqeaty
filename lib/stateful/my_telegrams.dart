import 'package:barkeaty/shared/models/my_letter_model.dart';
import 'package:barkeaty/shared/network/local/helpers/cache_helper.dart';
import 'package:barkeaty/shared/network/remote/service/my_service.dart';
import 'package:barkeaty/stateful/home.dart';
import 'package:barkeaty/stateful/my_telegrams_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:flutter/material.dart';


class my_telegrams extends StatefulWidget {
  @override
  _my_telegramsState createState() => _my_telegramsState();
}

class _my_telegramsState extends State<my_telegrams> {

  var searchController = TextEditingController();
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
        return Scaffold(
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            child: SizedBox(
              height: 100.h,
              child: Column(
                children: [
                  Stack(
                    children: [
                      Container(
                        color: const Color.fromRGBO(1, 18, 112, 1),
                        height: 35.h,
                      ),
                      Positioned(
                        top: 2.h,
                        left: 30.w,
                        child: Image(
                            width: 50.w,
                            height: 25.h,
                            image: const AssetImage("android/assets/barqiaty.png")),
                      ),
                      Positioned(
                        top: 23.5.h,
                        left: 2.w,
                        child: GestureDetector(
                          onTap: (){
                            Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                builder: (context) => Home(),
                              ),
                                  (route) {
                                return false;
                              },
                            );
                          },
                          child: Image(
                              width: 15.w,
                              height: 15.w,
                              image: const AssetImage("android/assets/straight-right-arrow.png")),
                        ),
                      ),
                      Positioned(
                        top: 25.h,
                        left: 18.w,
                        child:  Container(
                          width: 57.w,
                          height: 4.5.h,
                          decoration: const BoxDecoration(
                            //color: Color.fromRGBO(245, 153, 45, 1),
                            borderRadius: BorderRadius.all(Radius.circular(50.0)),
                          ),
                          child: TextFormField(
                            controller: searchController,
                            onChanged: onSearchTextChanged,
                            textAlign: TextAlign.center,
                            enabled: true,
                            style: TextStyle(
                              fontFamily: 'theFont',
                              fontSize: 4.w,
                              color: Colors.white,
                            ),
                            cursorColor: const Color.fromRGBO(3, 141, 151, 1),
                            decoration: InputDecoration(
                              prefixIcon: Padding(
                                padding: EdgeInsets.zero, // add padding to adjust icon
                                child: Icon(Icons.search,
                                  color: Colors.white,
                                  size: 7.w,),
                              ),
                              contentPadding: EdgeInsets.all(1.w),
                              isDense: true,
                              fillColor: const Color.fromRGBO(112, 116, 162, 1),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(50.0),
                              ),
                              filled: true,
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        top: 25.h,
                        left: 80.w,
                        child: Text("برقياتي",style: TextStyle(
                          fontSize: 5.w,
                          fontFamily: "theFont",
                          color: Colors.white,
                        ),),),
                    ],
                  ),
                  Expanded(
                    child: _searchResult.length != 0 || searchController.text.isNotEmpty
                        ? ListView.builder(
                      shrinkWrap: true,
                      itemBuilder: (context, i) => loading == false ? InkWell(
                        onTap: () async{
                          setState(() {
                            loading = true;
                          });
                          await Provider.of<MyService>(context,listen: false).getMyLettersInfo(
                              _searchResult[i].id
                          );
                          setState(() {
                            loading = false;
                          });
                          if(Provider.of<MyService>(context,listen: false).getMyLettersInfoResp!.status == 200){
                            CacheHelper.saveData(key: 'myTelSts', value: Provider.of<MyService>(context,listen: false).getMyLettersResp!.letters[i].status.name);
                            Navigator.push(context,
                              MaterialPageRoute(
                                  builder: (context) => const MyTelegramsInfo()),
                            );
                          }
                        },
                        child: Container(
                          margin: const EdgeInsets.only(top: 0, left: 25, right: 25, bottom: 15,),
                          padding: const EdgeInsets.only(right: 15),
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.35),
                                  spreadRadius: 5,
                                  blurRadius: 7,
                                  offset: Offset(0, 3), // changes position of shadow
                                ),
                              ],
                              borderRadius: BorderRadius.circular(15),
                              color: Colors.white),
                          child: Column(
                            children: [
                              Container(
                                padding: EdgeInsets.all(2.w),
                                decoration: const BoxDecoration(
                                  color: Color.fromRGBO(1, 18, 112, 1),
                                  borderRadius: BorderRadius.only(bottomRight: Radius.circular(15.0), bottomLeft: Radius.circular(15.0)),
                                ),
                                child: Text(
                                  _searchResult[i].createdAt.toString(),
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(
                                    color: Color.fromRGBO(105, 203, 248, 1),
                                  ),
                                ),
                              ),
                              SizedBox(height: 1.h,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    'رقم البرقية: '+_searchResult[i].id.toString(),
                                    style: TextStyle(
                                        color: const Color.fromRGBO(
                                            1, 18, 112, 1),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 5.w
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 1.5.w,),
                              Text(
                               _searchResult[i].privateCompany == null
                                    ? _searchResult[i].direction!.name
                                    : _searchResult[i].privateCompany!.name,
                                textAlign: TextAlign.center,
                                style: const TextStyle(
                                  color: Color.fromRGBO(
                                      1, 18, 112, 1),
                                ),
                              ),
                              SizedBox(height: 1.h,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(1.w),
                                    decoration: BoxDecoration(
                                      color: Colors.grey[600],
                                      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(15.0),),
                                    ),
                                    child: Text(
                                      _searchResult[i].status.name,
                                      textAlign: TextAlign.center,
                                      style: const TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ) : Center(
                        child: Container(
                          width: 50.w,
                          height: 8.h,
                          margin: EdgeInsetsDirectional.only(start: 20.w),
                          alignment: Alignment.center,
                          child: Row(
                            children: const [
                              CircularProgressIndicator.adaptive(
                                backgroundColor: Color.fromRGBO(
                                    1, 18, 112, 1),
                              ),
                            ],
                          ),
                        ),
                      ),
                      itemCount: loading == false ? _searchResult.length : 1,
                    )
                        : ListView.builder(
                      shrinkWrap: true,
                      itemBuilder: (context, index) => loading == false ? InkWell(
                        onTap: () async{
                          setState(() {
                            loading = true;
                          });
                          await Provider.of<MyService>(context,listen: false).getMyLettersInfo(
                              Provider.of<MyService>(context,listen: false).getMyLettersResp!.letters[index].id
                          );
                          setState(() {
                            loading = false;
                          });
                          if(Provider.of<MyService>(context,listen: false).getMyLettersInfoResp!.status == 200){
                            CacheHelper.saveData(key: 'myTelSts', value: Provider.of<MyService>(context,listen: false).getMyLettersResp!.letters[index].status.name);
                            Navigator.push(context,
                              MaterialPageRoute(
                                  builder: (context) => const MyTelegramsInfo()),
                            );
                          }
                        },
                        child: Container(
                          margin: const EdgeInsets.only(top: 0, left: 25, right: 25, bottom: 15,),
                          padding: const EdgeInsets.only(right: 15),
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.35),
                                  spreadRadius: 5,
                                  blurRadius: 7,
                                  offset: Offset(0, 3), // changes position of shadow
                                ),
                              ],
                              borderRadius: BorderRadius.circular(15),
                              color: Colors.white),
                          child: Column(
                            children: [
                              Container(
                                padding: EdgeInsets.all(2.w),
                                decoration: const BoxDecoration(
                                  color: Color.fromRGBO(1, 18, 112, 1),
                                  borderRadius: BorderRadius.only(bottomRight: Radius.circular(15.0), bottomLeft: Radius.circular(15.0)),
                                ),
                                child: Text(
                                  Provider.of<MyService>(context,listen: false).getMyLettersResp!.letters[index].createdAt.toString(),
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(
                                    color: Color.fromRGBO(105, 203, 248, 1),
                                  ),
                                ),
                              ),
                              SizedBox(height: 1.h,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    'رقم البرقية: '+Provider.of<MyService>(context,listen: false).getMyLettersResp!.letters[index].id.toString(),
                                    style: TextStyle(
                                      color: const Color.fromRGBO(
                                          1, 18, 112, 1),
                                      fontWeight: FontWeight.bold,
                                      fontSize: 5.w
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 1.5.w,),
                              Text(
                                Provider.of<MyService>(context,listen: false).getMyLettersResp!.letters[index].privateCompany == null
                                    ? Provider.of<MyService>(context,listen: false).getMyLettersResp!.letters[index].direction!.name
                                    : Provider.of<MyService>(context,listen: false).getMyLettersResp!.letters[index].privateCompany!.name,
                                textAlign: TextAlign.center,
                                style: const TextStyle(
                                  color: Color.fromRGBO(
                                      1, 18, 112, 1),
                                ),
                              ),
                              SizedBox(height: 1.h,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(1.w),
                                    decoration: BoxDecoration(
                                      color: Colors.grey[600],
                                      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(15.0),),
                                    ),
                                    child: Text(
                                      Provider.of<MyService>(context,listen: false).getMyLettersResp!.letters[index].status.name,
                                      textAlign: TextAlign.center,
                                      style: const TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ) : Center(
                        child: Container(
                          width: 50.w,
                          height: 8.h,
                          margin: EdgeInsetsDirectional.only(start: 20.w),
                          alignment: Alignment.center,
                          child: Row(
                            children: const [
                              CircularProgressIndicator.adaptive(
                                backgroundColor: Color.fromRGBO(
                                    1, 18, 112, 1),
                              ),
                            ],
                          ),
                        ),
                      ),
                      itemCount: loading == false
                          ? Provider.of<MyService>(context,listen: false).getMyLettersResp!.letters.length
                          : 1,
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
  onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    Provider.of<MyService>(context,listen: false).getMyLettersResp!.letters.forEach((letter) {
      if (letter.direction != null && letter.direction!.name.contains(text) || letter.privateCompany != null && letter.privateCompany!.name.contains(text)) {
        _searchResult.add(letter);
      }
    });

    setState(() {});
  }
}

List<MyLetter> _searchResult = [];
