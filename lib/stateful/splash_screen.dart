import 'package:barkeaty/shared/network/local/helpers/cache_helper.dart';
import 'package:barkeaty/shared/network/remote/service/my_service.dart';
import 'package:barkeaty/stateful/sign_in.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:splash_screen_view/SplashScreenView.dart';

import 'home.dart';


class MySplashScreen extends StatefulWidget {
  const MySplashScreen({Key? key}) : super(key: key);

  @override
  _MySplashScreenState createState() => _MySplashScreenState();
}

class _MySplashScreenState extends State<MySplashScreen> {

  var token = CacheHelper.getData(key: 'token');

  @override
  Widget build(BuildContext context) {

    Provider.of<MyService>(context,listen: false).unseenNotifications();

    int sec = 3000;

    return Sizer(
      builder: (BuildContext context, Orientation orientation, DeviceType deviceType) {
        return Center(
          child: SplashScreenView(
            imageSize: 20.h.toInt(),
            duration: Provider.of<MyService>(context,listen: false).unseenNotificationsResp != null ? sec : sec+=1000,
            navigateRoute: token == null || token == "" ? sign_in() : Home(),
            imageSrc: "android/assets/barqiaty.png",
            backgroundColor: Colors.white,
          ),
        );
      },
    );
  }
}
