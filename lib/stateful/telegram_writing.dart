import 'package:barkeaty/shared/network/local/helpers/cache_helper.dart';
import 'package:barkeaty/shared/network/remote/service/my_service.dart';
import 'package:barkeaty/stateful/attachments.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';


class telegram_writing extends StatefulWidget {
  @override
  _telegram_writingState createState() => _telegram_writingState();
}

class _telegram_writingState extends State<telegram_writing> {
  var text_align = TextAlign.right;
  String hintVar = "... اكتب هنا";
  Color icon1Color = Colors.black26, icon2Color = Colors.black26, icon3Color = Colors.blue, boldColor = Colors.grey;
  var telegramController = TextEditingController();
  bool loading = false;


  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
        return Scaffold(
          backgroundColor: Colors.white,
          body: Stack(
            children: [
              Container(
                color: const Color.fromRGBO(1, 18, 112, 1),
                height: 35.h,
              ),
              SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        GestureDetector(
                          onTap: (){
                            Navigator.pop(context);
                          },
                          child: Image(
                              width: 15.w,
                              height: 15.w,
                              image: const AssetImage("android/assets/down-arrow-1.png")),
                        ),
                        SizedBox(width: 10.w),
                        Image(
                            width: 50.w,
                            height: 25.h,
                            image: const AssetImage("android/assets/barqiaty.png")),
                      ],
                    ),
                    Column(
                      children: [
                        Container(
                          padding: EdgeInsets.only(right: 2.h, left: 2.h, top: 1.h, bottom: 2.h),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 2.w,
                                blurRadius: 2.w,
                                offset: Offset(0, 1.5.w), // changes position of shadow
                              ),
                            ],
                          ),
                          width: 92.w,
                          child: Column(
                            children: [
                              Text(
                                "ادخال نص البرقية",
                                style: TextStyle(
                                  fontSize: 5.w,
                                  //color: Colors.white,
                                  fontFamily: "theFont",
                                  color: const Color.fromRGBO(1, 18, 112, 1),
                                ),
                              ),
                              SizedBox(height: 2.h,),
                              Container(
                                padding: EdgeInsetsDirectional.only(start: 3.w, top: 1.h, end: 3.w, bottom: 2.h),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(6.w),
                                  border: Border.all(color: Colors.black26, width: .5.w),),
                                width: 85.w,
                                height: 45.h,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        Container(
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(2.w),
                                            border: Border.all(color: Colors.black26, width: .5.w),),
                                          width: 40.w,
                                          height: 11.w,
                                          child: Row(
                                            children: [
                                              SizedBox(width: 2.5.w,),
                                              GestureDetector(
                                                  onTap: (){
                                                    setState(() {
                                                      text_align = TextAlign.left;
                                                      hintVar = "Right here...";
                                                      icon1Color = Colors.blue;
                                                      icon2Color = Colors.black26;
                                                      icon3Color = Colors.black26;
                                                    });
                                                  },
                                                  child: Icon(Icons.format_align_left,
                                                    color: icon1Color,size: 8.w,)),
                                              SizedBox(width: 2.5.w,),
                                              Container(
                                                height: 4.h,
                                                width: .5.w,
                                                color: Colors.black26,
                                              ),
                                              SizedBox(width: 2.5.w,),
                                              GestureDetector(
                                                  onTap: (){
                                                    setState(() {
                                                      text_align = TextAlign.center;
                                                      hintVar = "... اكتب هنا";
                                                      icon1Color = Colors.black26;
                                                      icon2Color = Colors.blue;
                                                      icon3Color = Colors.black26;
                                                    });
                                                  },
                                                  child: Icon(Icons.format_align_center,
                                                    color: icon2Color,size: 8.w,)),
                                              SizedBox(width: 2.5.w,),
                                              Container(
                                                height: 4.h,
                                                width: .5.w,
                                                color: Colors.black26,
                                              ),
                                              SizedBox(width: 2.5.w,),
                                              GestureDetector(
                                                  onTap: (){
                                                    setState(() {
                                                      text_align = TextAlign.right;
                                                      hintVar = "... اكتب هنا";
                                                      icon1Color = Colors.black26;
                                                      icon2Color = Colors.black26;
                                                      icon3Color = Colors.blue;
                                                    });
                                                  },
                                                  child: Icon(Icons.format_align_right,
                                                    color: icon3Color,size: 8.w,)),
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 3.w,),
                                        Row(
                                          children: [
                                            Image(image: const AssetImage("android/assets/text-along-path.png"), width: 5.w, height: 5.w,),
                                            SizedBox(width: 3.w,),
                                            GestureDetector(
                                                onTap: (){
                                                  setState(() {
                                                    if(boldColor == Colors.grey){
                                                      boldColor = Color.fromRGBO(105, 203, 248, 1);
                                                    }
                                                    else{
                                                      boldColor = Colors.grey;
                                                    }
                                                  });
                                                },
                                                child: SizedBox(
                                                    width: 8.w,
                                                    height: 5.h,
                                                    //color: Colors.red,
                                                    child: Image(image: const AssetImage("android/assets/bold-text 1.png"), color: boldColor ,width: 5.w, height: 5.w,))),
                                          ],
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 1.h,),
                                    Container(
                                      color: Colors.black26,
                                      height: .5.w,
                                      width: 80.w,
                                    ),
                                    SizedBox(height: 1.h,),
                                    Container(
                                      width: 75.w,
                                      height: 33.h,
                                      child: TextFormField(
                                        onChanged: (val){},
                                        textAlign: text_align,
                                        scrollPhysics: const ScrollPhysics(),
                                        keyboardType: TextInputType.multiline,
                                        enabled: true,
                                        maxLines: null,
                                        controller: telegramController,
                                        style: TextStyle(
                                          fontFamily: 'theFont',
                                          fontSize: 5.w,
                                          color: Colors.black,
                                        ),
                                        cursorColor: Colors.black26,
                                        decoration: InputDecoration(
                                          hintText: hintVar,
                                          hintStyle: const TextStyle(
                                            color: Colors.black26,
                                            fontFamily: "theFont",
                                          ),
                                          enabledBorder: InputBorder.none,
                                          focusedBorder: InputBorder.none,
                                          disabledBorder: InputBorder.none,
                                          errorBorder: InputBorder.none,
                                          contentPadding: EdgeInsets.all(1.w),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: 2.h,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  GestureDetector(
                                    onTap: (){
                                      if(telegramController.text.isEmpty){
                                        Fluttertoast.showToast(
                                            msg: 'لا يمكن ارسال برقية فارغة.',
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.CENTER,
                                            timeInSecForIosWeb: 2,
                                            backgroundColor: const Color.fromRGBO(
                                                1, 18, 112, 1),
                                            textColor: Colors.white,
                                            fontSize: 16.0
                                        );
                                      }else {
                                        CacheHelper.saveData(key: 'letterText', value: telegramController.text);
                                        Navigator.push(context,
                                          MaterialPageRoute(
                                              builder: (context) => attachments()),
                                        );
                                      }
                                    },
                                    child: Container(
                                      padding: EdgeInsets.only(left: 4.w, right: 4.w),
                                      alignment: Alignment.center,
                                      decoration: const BoxDecoration(
                                        color: Color.fromRGBO(105, 203, 248, 1),
                                        borderRadius: BorderRadius.all(Radius.circular(50.0)),
                                      ),
                                      child: Text(
                                        "التالي",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                        fontSize: 6.5.w,
                                        color: Colors.white,
                                        fontFamily: "theFont",
                                      ),),
                                    ),
                                  ),
                                  SizedBox(width: 3.w,),
                                  GestureDetector(
                                    onTap: (){
                                      Navigator.pop(context);
                                    },
                                    child: Container(
                                      padding: EdgeInsets.only(left: 4.w, right: 4.w),
                                      alignment: Alignment.center,
                                      decoration: const BoxDecoration(
                                        color: Color.fromRGBO(105, 203, 248, 1),
                                        borderRadius: BorderRadius.all(Radius.circular(50.0)),
                                      ),
                                      child: Text(
                                        "السابق",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                        fontSize: 6.5.w,
                                        color: Colors.white,
                                        fontFamily: "theFont",
                                      ),),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 3.h,),
                    Container(
                      padding: EdgeInsets.only(left: 2.w, right: 2.w,),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: Colors.black,
                          width: 0.3.w,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(5.w)),
                      ),
                      child: Text("نماذج البرقيات",
                        style: TextStyle(
                        fontSize: 5.w,
                        fontFamily: "theFont",
                        color: const Color.fromRGBO(1, 18, 112, 1),
                      ),),
                    ),
                    SizedBox(height: 0.5.h,),
                    Flexible(
                      fit: FlexFit.loose,
                      child: ListView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemBuilder: (context, index) => InkWell(
                          onTap: () async{
                            telegramController.text = Provider.of<MyService>(context,listen: false).getTemplatesResp!.templates[index].text;
                            setState(() {});
                          },
                          child: Container(
                            margin: const EdgeInsets.only(top: 0, left: 25, right: 25, bottom: 15,),
                            padding: const EdgeInsets.all(20),
                            decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.35),
                                    spreadRadius: 5,
                                    blurRadius: 7,
                                    offset: Offset(0, 3), // changes position of shadow
                                  ),
                                ],
                                borderRadius: BorderRadius.circular(15),
                                color: Colors.white),
                            child: Column(
                              children: [
                                Text(
                                  Provider.of<MyService>(context,listen: false).getTemplatesResp!.templates[index].name,
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(
                                    color: Color.fromRGBO(
                                        1, 18, 112, 1),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        itemCount: Provider.of<MyService>(context,listen: false).getTemplatesResp!.templates.length,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
