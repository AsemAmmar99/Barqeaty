import 'package:barkeaty/shared/network/local/helpers/cache_helper.dart';
import 'package:barkeaty/shared/network/remote/service/my_service.dart';
import 'package:barkeaty/stateful/destination.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

class service_type extends StatefulWidget {
  @override
  _service_typeState createState() => _service_typeState();
}

class _service_typeState extends State<service_type> {
  Color color = Colors.white;
  Color color1 = Color.fromRGBO(105, 203, 248, 1);
  bool govEntity = false;
  bool privateCompany = false;
  bool loading = false;
  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
        return Scaffold(
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            child: SizedBox(
              height: 100.h,
              child: Stack(
                children: [
                  Container(
                    color: const Color.fromRGBO(1, 18, 112, 1),
                    height: 35.h,
                  ),
                  Positioned(
                    top: 2.h,
                    left: 30.w,
                    child: Image(
                        width: 50.w,
                        height: 25.h,
                        image: AssetImage("android/assets/barqiaty.png")),
                  ),
                  Positioned(
                    top: 10.h,
                    left: 6.w,
                    child: GestureDetector(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Image(
                          width: 15.w,
                          height: 15.w,
                          image: AssetImage("android/assets/down-arrow-1.png")),
                    ),
                  ),
                  Positioned(
                    top: 25.h,
                    left: 3.5.w,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      width: 92.w,
                      height: 65.h,
                    ),
                  ),
                  Positioned(
                    top: 26.h,
                    left: 24.w,
                    child: Text("الرجاء اختيار نوع الخدمة",style: TextStyle(
                      fontSize: 5.5.w,
                      //color: Colors.white,
                      fontFamily: "theFont",
                      color: const Color.fromRGBO(1, 18, 112, 1),
                    ),),
                ),
                  Positioned(
                    top: 62.h,
                    left: 8.w,
                    child: Container(
                      height: 10.h,
                      width: 80.w,
                      alignment: AlignmentDirectional.centerEnd,
                      child: Text(Provider.of<MyService>(context,listen: false).getLetterBeforeStartResp!.termsData.text,
                        textAlign: TextAlign.end,
                        style: TextStyle(
                        fontSize: 4.w,
                        //color: Colors.white,
                        fontFamily: "theFont",
                        color: const Color.fromRGBO(1, 18, 112, 1),
                      ),),
                    ),
                  ),
                  Positioned(
                    top: 75.h,
                    left: 12.w,
                    child: loading == false ? GestureDetector(
                      onTap: () async{
                        setState(() {
                          loading = true;
                        });
                        await Provider.of<MyService>(context,listen: false).getDirections();
                        await Provider.of<MyService>(context,listen: false).getPrivateCompanies();
                        setState(() {
                          loading = false;
                        });
                        if(privateCompany == false && govEntity == false) {
                          Fluttertoast.showToast(
                              msg: 'اختر نوع الخدمة.',
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 2,
                              backgroundColor: const Color.fromRGBO(
                                  1, 18, 112, 1),
                              textColor: Colors.white,
                              fontSize: 16.0
                          );
                        }else if(Provider.of<MyService>(context,listen: false).getDirectionsResp!.status == 200 && Provider.of<MyService>(context,listen: false).getPrivateCompaniesResp!.status == 200){
                          Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                              builder: (context) => destination(),
                            ),
                                (route) {
                              return false;
                            },
                          );
                        }
                      },
                      child: Container(
                        width: 35.w,
                        height: 7.h,
                        alignment: Alignment.center,
                        decoration: const BoxDecoration(
                          color: Color.fromRGBO(105, 203, 248, 1),
                          borderRadius: BorderRadius.all(Radius.circular(50.0)),
                        ),
                        child: Text("التالي",style: TextStyle(
                          fontSize: 6.5.w,
                          color: Colors.white,
                          fontFamily: "theFont",
                        ),),
                      ),
                    ) : Center(
                      child: Container(
                        width: 50.w,
                        height: 8.h,
                        margin: EdgeInsetsDirectional.only(start: 20.w),
                        alignment: Alignment.center,
                        child: Row(
                          children: const [
                            CircularProgressIndicator.adaptive(
                              backgroundColor: Color.fromRGBO(
                                  1, 18, 112, 1),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 75.h,
                    left: 51.w,
                    child: GestureDetector(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Container(
                        width: 35.w,
                        height: 7.h,
                        alignment: Alignment.center,
                        decoration: const BoxDecoration(
                          color: Color.fromRGBO(105, 203, 248, 1),
                          borderRadius: BorderRadius.all(Radius.circular(50.0)),
                        ),
                        child: Text("السابق",style: TextStyle(
                          fontSize: 6.5.w,
                          color: Colors.white,
                          fontFamily: "theFont",
                        ),),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 40.h,
                    left: 53.w,
                    child: GestureDetector(
                      onTap: (){
                        setState(() {
                          if(govEntity == false){
                            privateCompany = false;
                            govEntity = true;
                            CacheHelper.saveData(key: 'serviceid', value: 5);
                          }
                        });
                      },
                      child: Container(
                        width: 18.h,
                        height: 18.h,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: govEntity == true ? color1 : color,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.all(Radius.circular(3.w,)),
                          border: Border.all(
                            color: const Color.fromRGBO(105, 203, 248, 1),
                            width: .5.w,
                          ),
                        ),
                        child: Text("${Provider.of<MyService>(context,listen: false).getServicesGovResp!.services[1].name} \n      ${Provider.of<MyService>(context,listen: false).getServicesGovResp!.services[1].minCost} \n      ريال \n    للبرقية  ",style: TextStyle(
                          fontFamily: "theFont",
                          fontSize: 4.5.w,
                          color: govEntity == true ? color : color1,
                        ),),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 40.h,
                    left: 8.w,
                    child: GestureDetector(
                      onTap: (){
                        setState(() {
                          if(privateCompany == false){
                            govEntity = false;
                            privateCompany = true;
                            CacheHelper.saveData(key: 'serviceid', value: 6);
                          }
                        });
                      },
                      child: Container(
                        width: 18.h,
                        height: 18.h,
                        decoration: BoxDecoration(
                          color: privateCompany == true ? color1 : color,
                          border: Border.all(
                            color: const Color.fromRGBO(105, 203, 248, 1),
                            width: .5.w,
                          ),
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.all(Radius.circular(3.w,)),
                        ),
                        alignment: Alignment.center,
                        child: Text("${Provider.of<MyService>(context,listen: false).getServicesCompResp!.services[1].name} \n      ${Provider.of<MyService>(context,listen: false).getServicesCompResp!.services[1].minCost} \n      ريال \n    للبرقية  ",style: TextStyle(
                          fontFamily: "theFont",
                          fontSize: 4.5.w,
                          color: privateCompany == true ? color : color1,
                        ),),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
