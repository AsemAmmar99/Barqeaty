import 'package:barkeaty/shared/network/remote/service/my_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

class Notifications extends StatefulWidget {
  const Notifications({Key? key}) : super(key: key);

  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  @override
  Widget build(BuildContext context) {
    return Sizer(
        builder: (BuildContext context, Orientation orientation, DeviceType deviceType) {
        return Scaffold(
          body: Container(
            padding: EdgeInsets.all(10.h),
            color: const Color.fromRGBO(105, 203, 248, 1),
            child: Column(
              children: [
                Expanded(
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemBuilder: (context, index) => Container(
                      margin: const EdgeInsets.only(top: 0, left: 25, right: 25, bottom: 15,),
                      padding: const EdgeInsets.all(15),
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.35),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: Offset(0, 3), // changes position of shadow
                            ),
                          ],
                          borderRadius: BorderRadius.circular(15),
                          color: Colors.white),
                      child: Column(
                        children: [
                          Text(
                            Provider.of<MyService>(context,listen: false).notificationsResp!.notifications[index].createdAt.toString(),
                            textAlign: TextAlign.center,
                            style: const TextStyle(
                              color: Color.fromRGBO(105, 203, 248, 1),
                            ),
                          ),
                          SizedBox(height: 5.h,),
                          Text(
                            Provider.of<MyService>(context,listen: false).notificationsResp!.notifications[index].content,
                            textAlign: TextAlign.center,
                            style: const TextStyle(
                              color: Color.fromRGBO(
                                  1, 18, 112, 1),
                            ),
                          ),
                        ],
                      ),
                    ),
                    itemCount: Provider.of<MyService>(context,listen: false).notificationsResp!.notifications.length,
                  ),
                ),
              ],
            ),
          ),
        );
    }
    );
  }
}
