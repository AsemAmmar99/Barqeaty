import 'package:barkeaty/shared/network/local/helpers/cache_helper.dart';
import 'package:barkeaty/shared/network/remote/service/my_service.dart';
import 'package:barkeaty/shared/network/remote/responses/login_response.dart';
import 'package:barkeaty/stateful/home.dart';
import 'package:barkeaty/stateful/personal_data.dart';
import 'package:barkeaty/stateful/verification_code.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:sizer/sizer.dart';
import 'package:provider/provider.dart';

class sign_in extends StatefulWidget {

  @override
  State<sign_in> createState() => _sign_inState();
}

class _sign_inState extends State<sign_in> {
  var idNumberController = TextEditingController();

  var formKey = GlobalKey<FormState>();

  bool loading = false;

  bool signUpLoading = false;

  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {


        return Scaffold(
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  Row(
                    children: [
                      SizedBox(width: 30.w,),
                      Image(
                          width: 50.w,
                          height: 25.h,
                          image: AssetImage("android/assets/barqiaty.png")),
                    ],
                  ),
                  Row(
                    children: [
                      SizedBox(width: 35.w,),
                      Text("تسجيل الدخول",
                        style: TextStyle(
                          color: const Color.fromRGBO(1, 18, 112, 1),
                          fontFamily: "theFont",
                          fontSize: 9.w,
                        ),),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text("برجاء تسجيل الدخول للاستمرار",
                        style: TextStyle(
                          color:  const Color.fromRGBO(112, 116, 162, 1),
                          fontFamily: "theFont",
                          fontSize: 4.8.w,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 8.h,),
                  Row(
                    children: [
                      SizedBox(width: 50.w,),
                      Text("رقم الهوية/الاقامة",
                        style: TextStyle(
                          color:  const Color.fromRGBO(112, 116, 162, 1),
                          fontFamily: "theFont",
                          fontSize: 4.8.w,
                        ),),
                    ],
                  ),
                  SizedBox(height: 2.h,),
                  Row(
                    children: [
                      SizedBox(width: 15.w,),
                      Container(
                        width: 70.w,
                        decoration: const BoxDecoration(
                          //color: Color.fromRGBO(245, 153, 45, 1),
                          borderRadius: BorderRadius.all(Radius.circular(25.0)),
                        ),
                        child: TextFormField(
                          onChanged: (val){},
                          textAlign: TextAlign.right,
                          keyboardType: TextInputType.number,
                          enabled: true,
                          controller: idNumberController,
                          validator: (value){
                            if (value == null || value.isEmpty) {
                              return '.أدخل رقم الهوية/الاقامة';
                            }
                          },
                          style: TextStyle(
                            fontFamily: 'theFont',
                            fontSize: 4.w,
                            color: Colors.white,
                          ),
                          cursorColor: const Color.fromRGBO(3, 141, 151, 1),
                          decoration: InputDecoration(
                            suffixIcon: Image(
                              image: const AssetImage("android/assets/driver-license.png"),
                              width: 3.w,
                              height: 3.w,
                            ),
                            contentPadding: EdgeInsets.all(1.w),
                            isDense: true,
                            fillColor: const Color.fromRGBO(1, 18, 112, 1),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            filled: true,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 5.h,),
                  Row(
                    children: [
                      SizedBox(width: 25.w,),
                      GestureDetector(
                        onTap: ()async{
                          if(formKey.currentState!.validate()) {
                            setState(() {
                              loading = true;
                            });
                          await  Provider.of<MyService>(context,listen: false).userLogin(idNumber: idNumberController.text);
                          if (Provider
                              .of<MyService>(context, listen: false).login!.status == 200) {
                            Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                builder: (context) => code(),
                              ),
                                  (route) {
                                return false;
                              },
                            );
                          } else {
                            setState(() {
                              loading = false;
                              Fluttertoast.showToast(
                                  msg: Provider
                                      .of<MyService>(context, listen: false)
                                      .login!
                                      .message,
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 2,
                                  backgroundColor: const Color.fromRGBO(
                                      1, 18, 112, 1),
                                  textColor: Colors.white,
                                  fontSize: 16.0
                              );
                            });
                          }
                          }
                        },
                        child: loading == false ? Container(
                          width: 50.w,
                          height: 8.h,
                          alignment: Alignment.center,
                          decoration: const BoxDecoration(
                            color: Color.fromRGBO(105, 203, 248, 1),
                            borderRadius: BorderRadius.all(Radius.circular(50.0)),
                          ),
                          child: Text("تسجيل الدخول",style: TextStyle(
                            fontSize: 6.5.w,
                            color: Colors.white,
                            fontFamily: "theFont",
                          ),),
                        ) : Center(
                          child: Container(
                            width: 50.w,
                            height: 8.h,
                            margin: EdgeInsetsDirectional.only(start: 20.w),
                            alignment: Alignment.center,
                            child: Row(
                              children: const [
                                CircularProgressIndicator.adaptive(
                                  backgroundColor: Color.fromRGBO(
                                      1, 18, 112, 1),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 8.h,),
                  signUpLoading == false ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextButton(onPressed: () async {
                        setState(() {
                          signUpLoading = true;
                        });
                        await  Provider.of<MyService>(context,listen: false).getRegions();
                        if(Provider.of<MyService>(context,listen: false).regions!.status == 200) {
                          setState(() {
                            signUpLoading = false;
                          });
                          Navigator.push(context,
                          MaterialPageRoute(
                              builder: (context) => personal_data()),
                        );
                        }else{
                          setState(() {
                            signUpLoading = false;
                          });
                        }
                      }, child: Text(
                        '!أنشئ حساب الأن',
                        style: TextStyle(
                          fontFamily: "theFont",
                          fontSize: 4.8.w,
                        ),
                      ),
                      ),
                      SizedBox(width: 7.w,),
                      Text(
                        'ليس لديك حساب؟',
                        style: TextStyle(
                          color:  const Color.fromRGBO(112, 116, 162, 1),
                          fontFamily: "theFont",
                          fontSize: 4.8.w,
                        ),
                      ),
                    ],
                  ) : Center(
                    child: Container(
                      width: 50.w,
                      height: 8.h,
                      margin: EdgeInsetsDirectional.only(start: 20.w),
                      alignment: Alignment.center,
                      child: Row(
                        children: const [
                          CircularProgressIndicator.adaptive(
                            backgroundColor: Color.fromRGBO(
                                1, 18, 112, 1),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
