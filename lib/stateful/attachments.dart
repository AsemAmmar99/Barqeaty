import 'dart:convert';
import 'dart:io';

import 'package:barkeaty/shared/network/local/helpers/cache_helper.dart';
import 'package:barkeaty/shared/network/remote/service/my_service.dart';
import 'package:barkeaty/stateful/payment.dart';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

class attachments extends StatefulWidget {
  @override
  _attachmentsState createState() => _attachmentsState();
}

class _attachmentsState extends State<attachments> {

  int attachmentsNum = 0;
  File? file1;
  File? file2;
  File? file3;
  File? file4;
  File? file5;
  File? file6;
  bool loading = false;
  List<File> files = [];
  List encodedList = [];
  Color buCol = Color.fromRGBO(105, 203, 248, 1);
  String? buText = 'اختيار الملفات';
  List multipartList = [];

  MultipartFile? multipartFile;

  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
        return Scaffold(
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            child: SizedBox(
              height: 100.h,
              child: Stack(
                children: [
                  Container(
                    color: const Color.fromRGBO(1, 18, 112, 1),
                    height: 35.h,
                  ),
                  Positioned(
                    top: 2.h,
                    left: 30.w,
                    child: Image(
                        width: 50.w,
                        height: 25.h,
                        image: const AssetImage("android/assets/barqiaty.png")),
                  ),
                  Positioned(
                    top: 10.h,
                    left: 6.w,
                    child: GestureDetector(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Image(
                          width: 15.w,
                          height: 15.w,
                          image: const AssetImage("android/assets/down-arrow-1.png")),
                    ),
                  ),
                  Positioned(
                    top: 25.h,
                    left: 3.5.w,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      width: 92.w,
                      height: 70.h,
                      child: Column(
                        children: [
                          SizedBox(height: 2.h,),
                          Text("ماتريد ارفاقه",style: TextStyle(
                            fontSize: 5.w,
                            //color: Colors.white,
                            fontFamily: "theFont",
                            color: const Color.fromRGBO(1, 18, 112, 1),
                          ),),
                          SizedBox(height: 1.h),
                          Row(
                            mainAxisAlignment: files.isEmpty ? MainAxisAlignment.center : MainAxisAlignment.start,
                            children: [
                              SizedBox(width: 3.w,),
                              Visibility(
                                visible: files.isEmpty ? false : true,
                                child: GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      files.clear();
                                      multipartList.clear();
                                    });
                                  },
                                  child: Container(
                                    width: 40.w,
                                    height: 7.h,
                                    alignment: Alignment.center,
                                    decoration: const BoxDecoration(
                                      color: Colors.red,
                                      borderRadius: BorderRadius.all(Radius.circular(50.0)),
                                    ),
                                    child: Text('مسح الكل' ,
                                      style: TextStyle(
                                        fontSize: 5.w,
                                        color: Colors.white,
                                        fontFamily: "theFont",
                                      ),),
                                  ),
                                ),
                              ),
                              SizedBox(width: 3.w,),
                              GestureDetector(
                                onTap: () async{
                                  FilePickerResult? result = await FilePicker.platform.pickFiles(allowMultiple: true);
                                  if (result != null) {
                                    files = result.paths.map((path) => File(path!)).toList();
                                    await multipartConvert();
                                    setState(() {});
                                    multipartList.clear();
                                    multipartList.add(multipartFile);
                                  } else {
                                    Fluttertoast.showToast(
                                        msg: 'لم يتم اختيار ملفات للرفع.',
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.CENTER,
                                        timeInSecForIosWeb: 2,
                                        backgroundColor: const Color.fromRGBO(
                                            1, 18, 112, 1),
                                        textColor: Colors.white,
                                        fontSize: 16.0
                                    );
                                  }
                                },
                                child: Container(
                                  width: 40.w,
                                  height: 7.h,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    color: buCol,
                                    borderRadius: BorderRadius.all(Radius.circular(50.0)),
                                  ),
                                  child: Text(buText! ,
                                    style: TextStyle(
                                    fontSize: 5.w,
                                    color: Colors.white,
                                    fontFamily: "theFont",
                                  ),),
                                ),
                              ),
                              SizedBox(width: 5.w,),
                            ],
                          ),
                          files.isEmpty ? Container(
                            alignment: Alignment.center,
                            margin: EdgeInsets.all(10.w,),
                            child: Text(
                              '.لم يتم اختيار مرفقات بعد',
                              style: TextStyle(
                                color: Colors.red,
                                fontSize: 6.w,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ) : Container(
                            height: 40.h,
                            child: ListView.builder(
                              shrinkWrap: true,
                              itemBuilder: (context, index) => Container(
                                margin: const EdgeInsets.only(top: 0, left: 15, right: 15, bottom: 15,),
                                padding: EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.35),
                                        spreadRadius: 5,
                                        blurRadius: 7,
                                        offset: Offset(0, 3), // changes position of shadow
                                      ),
                                    ],
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.white),
                                child: Column(
                                  children: [
                                    Text(
                                      files[index].path.split('/').last,
                                      textAlign: TextAlign.center,
                                      style: const TextStyle(
                                        color: Color.fromRGBO(
                                            1, 18, 112, 1),
                                      ),
                                    ),
                                    SizedBox(height: 1.h,),
                                    GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          files.removeAt(index);
                                          multipartList.removeAt(index);
                                        });
                                      },
                                      child: Container(
                                        width: 35.w,
                                        height: 5.h,
                                        padding: EdgeInsets.only(bottom: 1.w),
                                        alignment: Alignment.center,
                                        decoration: const BoxDecoration(
                                          color: Colors.red,
                                          borderRadius: BorderRadius.all(Radius.circular(50.0)),
                                        ),
                                        child: Text('مسح المرفق' ,
                                          style: TextStyle(
                                            fontSize: 5.w,
                                            color: Colors.white,
                                            fontFamily: "theFont",
                                          ),),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              itemCount: files.length,
                            ),
                          ),
                          // Row(
                          //   children: [
                          //     SizedBox(width: 3.w,),
                          //     Container(
                          //       decoration: BoxDecoration(
                          //         borderRadius: BorderRadius.circular(6.w),
                          //         border: Border.all(color: Colors.black26, width: .8.w),),
                          //       width: 25.w,
                          //       height: 20.w,
                          //       child: Row(
                          //         children: [
                          //           Image(image: const AssetImage("android/assets/document.png"),width: 10.w,height: 10.w,),
                          //           Image(image: const AssetImage("android/assets/gallery.png"),width: 10.w,height: 10.w,),
                          //         ],
                          //       ),
                          //     ),
                          //     SizedBox(width: 5.w,),
                          //     Container(
                          //       decoration: BoxDecoration(
                          //         borderRadius: BorderRadius.circular(6.w),
                          //         border: Border.all(color: Colors.black26, width: .8.w),),
                          //       width: 25.w,
                          //       height: 20.w,
                          //       child: Row(
                          //         children: [
                          //           Image(image: const AssetImage("android/assets/document.png"),width: 10.w,height: 10.w,),
                          //           Image(image: const AssetImage("android/assets/gallery.png"),width: 10.w,height: 10.w,),
                          //         ],
                          //       ),
                          //     ),
                          //     SizedBox(width: 5.w,),
                          //     Container(
                          //       decoration: BoxDecoration(
                          //         borderRadius: BorderRadius.circular(6.w),
                          //         border: Border.all(color: Colors.black26, width: .8.w),),
                          //       width: 25.w,
                          //       height: 20.w,
                          //       child: Row(
                          //         children: [
                          //           Image(image: const AssetImage("android/assets/document.png"),width: 10.w,height: 10.w,),
                          //           Image(image: const AssetImage("android/assets/gallery.png"),width: 10.w,height: 10.w,),
                          //         ],
                          //       ),
                          //     ),
                          //   ],
                          // ),
                          // Row(
                          //   children: [
                          //     SizedBox(width: 10.w,),
                          //     file1 != null ? InkWell(
                          //       onTap: (){
                          //         file1 = null;
                          //         setState(() {});
                          //         attachmentsNum--;
                          //       },
                          //         child: Text('مسح', style: TextStyle(color: Colors.red[900], fontWeight: FontWeight.bold, fontSize: 5.w),)
                          //     )
                          //         : InkWell(
                          //       onTap: () async{
                          //         FilePickerResult? result1 = await FilePicker.platform.pickFiles();
                          //         if (result1 != null) {
                          //           file1 = File(result1.files.single.path!);
                          //           setState(() {});
                          //           attachmentsNum++;
                          //         }
                          //       },
                          //           child: Image(
                          //       image: const AssetImage("android/assets/+.png"), width: 10.w, height: 10.w,),
                          //         ),
                          //     SizedBox(width: 20.w,),
                          //     file2 != null ? InkWell(
                          //         onTap: (){
                          //           file2 = null;
                          //           setState(() {});
                          //           attachmentsNum--;
                          //         },
                          //         child: Text('مسح', style: TextStyle(color: Colors.red[900], fontWeight: FontWeight.bold, fontSize: 5.w),)
                          //     )
                          //         : InkWell(
                          //       onTap: () async{
                          //         FilePickerResult? result2 = await FilePicker.platform.pickFiles();
                          //         if (result2 != null) {
                          //           file2 = File(result2.files.single.path!);
                          //           setState(() {});
                          //           attachmentsNum++;
                          //         }
                          //       },
                          //       child: Image(
                          //         image: const AssetImage("android/assets/+.png"), width: 10.w, height: 10.w,),
                          //     ),
                          //     SizedBox(width: 20.w,),
                          //     file3 != null ? InkWell(
                          //         onTap: (){
                          //           file3 = null;
                          //           setState(() {});
                          //           attachmentsNum--;
                          //         },
                          //         child: Text('مسح', style: TextStyle(color: Colors.red[900], fontWeight: FontWeight.bold, fontSize: 5.w),)
                          //     )
                          //         : InkWell(
                          //       onTap: () async{
                          //         FilePickerResult? result3 = await FilePicker.platform.pickFiles();
                          //         if (result3 != null) {
                          //           file3 = File(result3.files.single.path!);
                          //           setState(() {});
                          //           attachmentsNum++;
                          //         }
                          //       },
                          //       child: Image(
                          //         image: const AssetImage("android/assets/+.png"), width: 10.w, height: 10.w,),
                          //     ),
                          //   ],
                          // ),
                          // Row(
                          //   children: [
                          //     SizedBox(width: 3.w,),
                          //     Container(
                          //       decoration: BoxDecoration(
                          //         borderRadius: BorderRadius.circular(6.w),
                          //         border: Border.all(color: Colors.black26, width: .8.w),),
                          //       width: 25.w,
                          //       height: 20.w,
                          //       child: Row(
                          //         children: [
                          //           Image(image: AssetImage("android/assets/document.png"),width: 10.w,height: 10.w,),
                          //           Image(image: AssetImage("android/assets/gallery.png"),width: 10.w,height: 10.w,),
                          //         ],
                          //       ),
                          //     ),
                          //     SizedBox(width: 5.w,),
                          //     Container(
                          //       decoration: BoxDecoration(
                          //         borderRadius: BorderRadius.circular(6.w),
                          //         border: Border.all(color: Colors.black26, width: .8.w),),
                          //       width: 25.w,
                          //       height: 20.w,
                          //       child: Row(
                          //         children: [
                          //           Image(image: AssetImage("android/assets/document.png"),width: 10.w,height: 10.w,),
                          //           Image(image: AssetImage("android/assets/gallery.png"),width: 10.w,height: 10.w,),
                          //         ],
                          //       ),
                          //     ),
                          //     SizedBox(width: 5.w,),
                          //     Container(
                          //       decoration: BoxDecoration(
                          //         borderRadius: BorderRadius.circular(6.w),
                          //         border: Border.all(color: Colors.black26, width: .8.w),),
                          //       width: 25.w,
                          //       height: 20.w,
                          //       child: Row(
                          //         children: [
                          //           Image(image: AssetImage("android/assets/document.png"),width: 10.w,height: 10.w,),
                          //           Image(image: AssetImage("android/assets/gallery.png"),width: 10.w,height: 10.w,),
                          //         ],
                          //       ),
                          //     ),
                          //   ],
                          // ),
                          // Row(
                          //   children: [
                          //     SizedBox(width: 10.w,),
                          //     file4 != null ? InkWell(
                          //         onTap: (){
                          //           file4 = null;
                          //           setState(() {});
                          //           attachmentsNum--;
                          //         },
                          //         child: Text('مسح', style: TextStyle(color: Colors.red[900], fontWeight: FontWeight.bold, fontSize: 5.w),)
                          //     )
                          //         : InkWell(
                          //       onTap: () async{
                          //         FilePickerResult? result4 = await FilePicker.platform.pickFiles();
                          //         if (result4 != null) {
                          //           file4 = File(result4.files.single.path!);
                          //           setState(() {});
                          //           attachmentsNum++;
                          //         }
                          //       },
                          //       child: Image(
                          //         image: const AssetImage("android/assets/+.png"), width: 10.w, height: 10.w,),
                          //     ),
                          //     SizedBox(width: 20.w,),
                          //     file5 != null ? InkWell(
                          //         onTap: (){
                          //           file5 = null;
                          //           setState(() {});
                          //           attachmentsNum--;
                          //         },
                          //         child: Text('مسح', style: TextStyle(color: Colors.red[900], fontWeight: FontWeight.bold, fontSize: 5.w),)
                          //     )
                          //         : InkWell(
                          //       onTap: () async{
                          //         FilePickerResult? result5 = await FilePicker.platform.pickFiles();
                          //         if (result5 != null) {
                          //           file5 = File(result5.files.single.path!);
                          //           setState(() {});
                          //           attachmentsNum++;
                          //         }
                          //       },
                          //       child: Image(
                          //         image: const AssetImage("android/assets/+.png"), width: 10.w, height: 10.w,),
                          //     ),
                          //     SizedBox(width: 20.w,),
                          //     file6 != null ? InkWell(
                          //         onTap: (){
                          //           file6 = null;
                          //           setState(() {});
                          //           attachmentsNum--;
                          //         },
                          //         child: Text('مسح', style: TextStyle(color: Colors.red[900], fontWeight: FontWeight.bold, fontSize: 5.w),)
                          //     )
                          //         : InkWell(
                          //       onTap: () async{
                          //         FilePickerResult? result6 = await FilePicker.platform.pickFiles();
                          //         if (result6 != null) {
                          //           file6 = File(result6.files.single.path!);
                          //           setState(() {});
                          //           attachmentsNum++;
                          //         }
                          //       },
                          //       child: Image(
                          //
                          //         image: const AssetImage("android/assets/+.png"), width: 10.w, height: 10.w,),
                          //     ),
                          //   ],
                          // ),
                          SizedBox(height: 2.h),
                          Row(
                            children: [
                              SizedBox(width: 5.w,),
                              GestureDetector(
                              onTap: () async{
                                print(multipartList);
                                // for (var i = 0; i < files.length; i++){
                                //   final bytes = File(files[i].path).readAsBytesSync();
                                //   String file64 = base64Encode(bytes);
                                //   encodedList.add(file64);
                                //   print(encodedList);
                                // }
                                attachmentsNum = files.length;
                                CacheHelper.saveData(key: 'attsCount', value: attachmentsNum);
                                showDialog();
                              },
                              child: Container(
                                width: 40.w,
                                height: 7.h,
                                alignment: Alignment.center,
                                decoration: const BoxDecoration(
                                  color: Color.fromRGBO(105, 203, 248, 1),
                                  borderRadius: BorderRadius.all(Radius.circular(50.0)),
                                ),
                                child: Text("الانتقال للدفع",style: TextStyle(
                                  fontSize: 5.w,
                                  color: Colors.white,
                                  fontFamily: "theFont",
                                ),),
                              ),
                            ),
                              SizedBox(width: 5.w,),
                              GestureDetector(
                                onTap: (){
                                  Navigator.pop(context);
                                },
                                child: Container(
                                  width: 35.w,
                                  height: 7.h,
                                  alignment: Alignment.center,
                                  decoration: const BoxDecoration(
                                    color: Color.fromRGBO(105, 203, 248, 1),
                                    borderRadius: BorderRadius.all(Radius.circular(50.0)),
                                  ),
                                  child: Text("السابق",style: TextStyle(
                                    fontSize: 6.5.w,
                                    color: Colors.white,
                                    fontFamily: "theFont",
                                  ),),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Future multipartConvert() async{
    for(var i = 0; i < files.length; i++) {
      return multipartFile = MultipartFile.fromFileSync(files[i].path, filename: files[i].path.split('/').last);
    }
  }

  void showDialog() {
    showGeneralDialog(
      barrierLabel: "Barrier",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: const Duration(milliseconds: 700),
      context: context,
      pageBuilder: (_, __, ___) {
        return Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            height: 35.h,
            child: SizedBox.expand(
                  child: Column(
                    children: [
                      SizedBox(height: 3.h,),
                      Image(image: const AssetImage("android/assets/about.png"),width: 20.w,height: 20.w,),
                      SizedBox(height: 1.h,),
                      Material(
                        color: Colors.white,
                        child: Text("مراجعة المرفقات المرفوعة", style: TextStyle(
                            color: Colors.blue,
                            fontFamily: "theFont",
                            fontSize: 5.w,
                          ),),
                      ),
                      SizedBox(height: 1.h,),
                      Material(
                        color: Colors.white,
                        child: Text("$attachmentsNum " ":عدد المرفقات المرفوعة", style: TextStyle(
                          fontSize: 5.w,
                          fontFamily: "theFont",
                          decoration: TextDecoration.none,
                          color: Colors.red,
                        ),),
                      ),
                      SizedBox(height: 2.h,),
                      Row(
                        children: [
                          SizedBox(width: 5.w,),
                          loading == false ? GestureDetector(
                            onTap: () async{
                              setState(() {
                                loading = true;
                              });
                              await Provider.of<MyService>(context,listen: false).sendLetter(multipartList);
                              setState(() {
                                loading = false;
                              });
                              if(Provider.of<MyService>(context,listen: false).sendLetterResp!.status == 200)
                              {
                                Fluttertoast.showToast(
                                    msg: Provider.of<MyService>(context,listen: false).sendLetterResp!.message,
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 2,
                                    backgroundColor: const Color.fromRGBO(
                                        1, 18, 112, 1),
                                    textColor: Colors.white,
                                    fontSize: 16.0
                                );
                                Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => payment(),
                                  ),
                                      (route) {
                                    return false;
                                  },
                                );
                              }
                            },
                            child: Container(
                              width: 40.w,
                              height: 5.h,
                              alignment: Alignment.center,
                              decoration: const BoxDecoration(
                                color: Color.fromRGBO(105, 203, 248, 1),
                                borderRadius: BorderRadius.all(Radius.circular(50.0)),
                              ),
                              child: Material(
                                color: const Color.fromRGBO(105, 203, 248, 1),
                                child: Text("الانتقال للدفع",style: TextStyle(
                                  fontSize: 5.w,
                                  color: Colors.white,
                                  fontFamily: "theFont",
                                ),),
                              ),
                            ),
                          ) : Center(
                            child: Container(
                              width: 50.w,
                              height: 8.h,
                              alignment: Alignment.center,
                              child: Row(
                                children: const [
                                  CircularProgressIndicator.adaptive(
                                    backgroundColor: Color.fromRGBO(
                                        1, 18, 112, 1),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(width: 5.w,),
                          GestureDetector(
                            onTap: (){
                              Navigator.pop(context);
                            },
                            child: Container(
                              width: 40.w,
                              height: 5.h,
                              alignment: Alignment.center,
                              decoration: const BoxDecoration(
                                color: Colors.red,
                                borderRadius: BorderRadius.all(Radius.circular(50.0)),
                              ),
                              child: Material(
                                color: Colors.red,
                                child: Text("اضافة المزيد",style: TextStyle(
                                  fontSize: 5.w,
                                  color: Colors.white,
                                  fontFamily: "theFont",
                                ),),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
            color: Colors.white,
          ),
        );
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
  }
}
