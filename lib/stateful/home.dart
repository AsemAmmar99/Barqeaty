import 'package:barkeaty/shared/models/coupon_model.dart';
import 'package:barkeaty/shared/network/local/helpers/cache_helper.dart';
import 'package:barkeaty/shared/network/remote/service/my_service.dart';
import 'package:barkeaty/stateful/my_telegrams.dart';
import 'package:barkeaty/stateful/service_type.dart';
import 'package:barkeaty/stateful/sign_in.dart';
import 'package:barkeaty/stateless/about_app.dart';
import 'package:barkeaty/stateful/government_entity_list.dart';
import 'package:barkeaty/stateful/personal_companies_list.dart';
import 'package:barkeaty/stateless/return_policy.dart';
import 'package:barkeaty/stateless/telegram-terms.dart';
import 'package:barkeaty/stateful/telegram_templates.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

import 'notifications.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _bellVisability = true, _rectVisability = true, _rect2Visability = false;
  bool sendLoading = false;
  bool myTelLoading = false;
  bool notifyLoading = false;
  bool returnLoading = false;
  bool termsLoading = false;
  bool govLoading = false;
  bool compLoading = false;
  bool tempsLoading = false;
  bool aboutLoading = false;
  bool logoutLoading = false;

  @override
  Widget build(BuildContext context) {

    return Sizer(
      builder: (context, orientation, deviceType) {
        return Scaffold(
          key: _scaffoldKey,
          endDrawer: SizedBox(
            width: 73.w,
            child: Drawer(
              child: Container(
                color: const Color.fromRGBO(105, 203, 248, 1),
                child: Column(
                  children: [
                    SizedBox(height: 7.h,),
                    Row(
                      children: [
                        SizedBox(width: 6.w,),
                        GestureDetector(
                            onTap: (){
                              Navigator.of(context).pop();
                            },
                            child: Image(
                              width: 10.w,
                              height: 10.w,
                              image: const AssetImage("android/assets/menu-1.png"),
                            )),
                        SizedBox(width: 6.w,),
                        Text(CacheHelper.getData(key: 'name') ?? '',style: TextStyle(
                          fontSize: 5.w,
                          color: Colors.white,
                          fontFamily: "theFont",
                        ),
                        ),
                      ],
                    ),
                    SizedBox(height: 4.h,),
                    Container(
                      width: 60.w,
                      height: .5.w,
                      color: const Color.fromRGBO(1, 18, 112, .3),),
                    SizedBox(height: 4.h,),
                    returnLoading == false ? Row(
                      children: [
                        SizedBox(width: 23.5.w,),
                        GestureDetector(
                          onTap: () async{
                            setState(() {
                              returnLoading = true;
                            });
                            await Provider.of<MyService>(context,listen: false).getReturn();
                            setState(() {
                              returnLoading = false;
                            });
                            if(Provider.of<MyService>(context,listen: false).getReturnResp!.status == 200) {
                              Navigator.push(context,
                              MaterialPageRoute(builder: (context) => return_policy()),
                            );
                            }
                          },
                          child: Text("سياسة الارجاع",style: TextStyle(
                            fontSize: 5.w,
                            color: Colors.white,
                            fontFamily: "theFont",
                          ),
                          ),
                        ),
                        SizedBox(width: 5.w,),
                        Image(
                          width: 8.w,
                          height: 8.w,
                          image: const AssetImage("android/assets/return-box.png"),),
                      ],
                    ) : Center(
                      child: Container(
                        width: 50.w,
                        height: 8.h,
                        margin: EdgeInsetsDirectional.only(start: 20.w),
                        alignment: Alignment.center,
                        child: Row(
                          children: const [
                            CircularProgressIndicator.adaptive(
                              backgroundColor: Color.fromRGBO(
                                  1, 18, 112, 1),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 4.h,),
                    termsLoading == false ? Row(
                      children: [
                        SizedBox(width: 23.5.w,),
                        GestureDetector(
                          onTap: () async{
                            setState(() {
                              termsLoading = true;
                            });
                            await Provider.of<MyService>(context,listen: false).getLetterTerms();
                            setState(() {
                              termsLoading = false;
                            });
                            if(Provider.of<MyService>(context,listen: false).getLetterTermsResp!.status == 200) {
                              Navigator.push(context,
                              MaterialPageRoute(builder: (context) => telegram_terms()),
                            );
                            }
                          },
                          child: Text("شروط البرقية",style: TextStyle(
                            fontSize: 5.w,
                            color: Colors.white,
                            fontFamily: "theFont",
                          ),
                          ),
                        ),
                        SizedBox(width: 5.w,),
                        Image(
                          width: 8.w,
                          height: 8.w,
                          image: const AssetImage("android/assets/accept.png"),),
                      ],
                    ) : Center(
                      child: Container(
                        width: 50.w,
                        height: 8.h,
                        margin: EdgeInsetsDirectional.only(start: 20.w),
                        alignment: Alignment.center,
                        child: Row(
                          children: const [
                            CircularProgressIndicator.adaptive(
                              backgroundColor: Color.fromRGBO(
                                  1, 18, 112, 1),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 4.h,),
                    govLoading == false ? Row(
                      children: [
                        SizedBox(width: 7.w,),
                        GestureDetector(
                          onTap: () async{
                            setState(() {
                              govLoading = true;
                            });
                            await  Provider.of<MyService>(context,listen: false).getDirections();
                            setState(() {
                              govLoading = false;
                            });
                            if(Provider.of<MyService>(context,listen: false).getDirectionsResp!.status == 200) {
                              Navigator.push(context,
                                MaterialPageRoute(builder: (context) =>
                                    government_entity_list()),
                              );
                            }
                          },
                          child: Text("قائمة الجهات الحكومية",style: TextStyle(
                            fontSize: 5.w,
                            color: Colors.white,
                            fontFamily: "theFont",
                          ),
                          ),
                        ),
                        SizedBox(width: 5.w,),
                        Image(
                          width: 8.w,
                          height: 8.w,
                          image: const AssetImage("android/assets/shield.png"),),
                      ],
                    ) : Center(
                      child: Container(
                        width: 50.w,
                        height: 8.h,
                        margin: EdgeInsetsDirectional.only(start: 20.w),
                        alignment: Alignment.center,
                        child: Row(
                          children: const [
                            CircularProgressIndicator.adaptive(
                              backgroundColor: Color.fromRGBO(
                                  1, 18, 112, 1),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 4.h,),
                    compLoading == false ? Row(
                      children: [
                        SizedBox(width: 9.w,),
                        GestureDetector(
                          onTap: () async{
                            setState(() {
                              compLoading = true;
                            });
                            await  Provider.of<MyService>(context,listen: false).getPrivateCompanies();
                            setState(() {
                              compLoading = false;
                            });
                            if(Provider.of<MyService>(context,listen: false).getPrivateCompaniesResp!.status == 200) {
                              Navigator.push(context,
                                MaterialPageRoute(builder: (context) =>
                                    personal_companies_list()),
                              );
                            }
                          },
                          child: Text("قائمة الشركات الخاصة",style: TextStyle(
                            fontSize: 5.w,
                            color: Colors.white,
                            fontFamily: "theFont",
                          ),
                          ),
                        ),
                        SizedBox(width: 5.w,),
                        Image(
                          width: 8.w,
                          height: 8.w,
                          image: const AssetImage("android/assets/Bs.png"),),
                      ],
                    ) : Center(
                      child: Container(
                        width: 50.w,
                        height: 8.h,
                        margin: EdgeInsetsDirectional.only(start: 20.w),
                        alignment: Alignment.center,
                        child: Row(
                          children: const [
                            CircularProgressIndicator.adaptive(
                              backgroundColor: Color.fromRGBO(
                                  1, 18, 112, 1),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 4.h,),
                    tempsLoading == false ? Row(
                      children: [
                        SizedBox(width: 7.w,),
                        GestureDetector(
                          onTap: () async{
                            setState(() {
                              tempsLoading = true;
                            });
                            await Provider.of<MyService>(context,listen: false).getTemplates();
                            setState(() {
                              tempsLoading = false;
                            });
                            if(Provider.of<MyService>(context,listen: false).getTemplatesResp!.status == 200){
                              Navigator.push(context,
                                MaterialPageRoute(builder: (context) => telegram_templates()),
                              );
                            }
                          },
                          child: Text("نماذج البرقيات الجاهزة",style: TextStyle(
                            fontSize: 5.w,
                            color: Colors.white,
                            fontFamily: "theFont",
                          ),
                          ),
                        ),
                        SizedBox(width: 5.w,),
                        Image(
                          width: 8.w,
                          height: 8.w,
                          image: const AssetImage("android/assets/message (1).png"),),
                      ],
                    ) : Center(
                      child: Container(
                        width: 50.w,
                        height: 8.h,
                        margin: EdgeInsetsDirectional.only(start: 20.w),
                        alignment: Alignment.center,
                        child: Row(
                          children: const [
                            CircularProgressIndicator.adaptive(
                              backgroundColor: Color.fromRGBO(
                                  1, 18, 112, 1),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 4.h,),
                    aboutLoading == false ? Row(
                      children: [
                        SizedBox(width: 29.8.w,),
                        GestureDetector(
                             onTap: () async{
                               setState(() {
                                 aboutLoading = true;
                               });
                               await Provider.of<MyService>(context,listen: false).getAboutUs();
                               setState(() {
                                 aboutLoading = false;
                               });
                               if(Provider.of<MyService>(context,listen: false).getAboutUsResp!.status == 200) {
                                 Navigator.push(context,
                               MaterialPageRoute(builder: (context) => about_app()),
                            );
                               }
                             },
                          child: Text("عن التطبيق",style: TextStyle(
                            fontSize: 5.w,
                            color: Colors.white,
                            fontFamily: "theFont",
                          ),
                          ),
                        ),
                        SizedBox(width: 5.w,),
                        Image(
                          width: 8.w,
                          height: 8.w,
                          image: const AssetImage("android/assets/information.png"),),
                      ],
                    ) : Center(
                      child: Container(
                        width: 50.w,
                        height: 8.h,
                        margin: EdgeInsetsDirectional.only(start: 20.w),
                        alignment: Alignment.center,
                        child: Row(
                          children: const [
                            CircularProgressIndicator.adaptive(
                              backgroundColor: Color.fromRGBO(
                                  1, 18, 112, 1),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 4.h,),
                    logoutLoading == false ? Row(
                      children: [
                        SizedBox(width: 25.0.w,),
                        GestureDetector(
                          onTap: () async{
                            setState(() {
                              logoutLoading = true;
                            });
                            await  Provider.of<MyService>(context,listen: false).logout();
                            setState(() {
                              logoutLoading = false;
                            });
                            if(Provider.of<MyService>(context,listen: false).logoutResp!.status == 200) {
                              setState(() {
                                CacheHelper.removeData(key: 'token');
                                Fluttertoast.showToast(
                                    msg: Provider.of<MyService>(context,listen: false).logoutResp!.message,
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 2,
                                    backgroundColor: const Color.fromRGBO(
                                        1, 18, 112, 1),
                                    textColor: Colors.white,
                                    fontSize: 16.0
                                );
                                Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => sign_in(),
                                  ),
                                      (route) {
                                    return false;
                                  },
                                );
                              });
                            }else{
                              setState(() {
                                CacheHelper.removeData(key: 'token');
                                Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => sign_in(),
                                  ),
                                      (route) {
                                    return false;
                                  },
                                );
                              }
                              );
                            }
                          },
                        child: Text("تسجيل الخروج",style: TextStyle(
                          fontSize: 5.w,
                          color: Colors.white,
                          fontFamily: "theFont",
                        ),
                        ),
                        ),
                        SizedBox(width: 3.w,),
                        Image(
                          width: 8.w,
                          height: 8.w,
                          image: const AssetImage("android/assets/log-out.png"),),
                      ],
                    ) : Center(
                      child: Container(
                        width: 50.w,
                        height: 8.h,
                        margin: EdgeInsetsDirectional.only(start: 20.w),
                        alignment: Alignment.center,
                        child: Row(
                          children: const [
                            CircularProgressIndicator.adaptive(
                              backgroundColor: Color.fromRGBO(
                                  1, 18, 112, 1),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          body: SizedBox(
            child: Stack(
              children: [
                Row(
                  children: [
                    Container(
                      width: 100.w,
                      color: const Color.fromRGBO(1, 18, 112, 1), //blue
                    ),
                    // Container(width: 33.w,color: Colors.white,)
                  ],
                ),
                Positioned(
                  left: 24.w,
                  top: 10.h,
                  child: Image(
                      width: 50.w,
                      height: 25.h,
                      image: AssetImage("android/assets/barqiaty.png")),
                ),
                //ارسال برقية
                Positioned(
                    left: 13.w,
                    top: 30.h,
                    child: sendLoading == false ? GestureDetector(
                        onTap: () async {
                          setState(() {
                            sendLoading = true;
                          });
                          await Provider.of<MyService>(context, listen: false)
                              .getLetterBeforeStart();
                          await Provider.of<MyService>(context, listen: false)
                              .getServicesGov();
                          await Provider.of<MyService>(context, listen: false)
                              .getServicesComp();
                          setState(() {
                            sendLoading = false;
                          });
                          if (Provider.of<MyService>(context,listen: false).getLetterBeforeStartResp!.status == 200 && Provider.of<MyService>(context,listen: false).getServicesGovResp!.status == 200 && Provider.of<MyService>(context,listen: false).getServicesCompResp!.status == 200) {
                            sendLoading = false;
                            Navigator.push(context,
                              MaterialPageRoute(
                                  builder: (context) => service_type()),
                            );
                          }else{
                            sendLoading = false;
                          }
                          },
                        child: SizedBox(
                          child: Stack(
                            children: [
                              Image(
                                width: 73.w,
                                height: 35.w,
                                image: const AssetImage("android/assets/Rectangle 72.png"),
                              ),
                              Positioned(
                                left: 18.w,
                                top: 5.h,
                                child: Text("إرسال برقية",style: TextStyle(
                                  fontFamily: "theFont",
                                  color: const Color.fromRGBO(1, 18, 112, 1),
                                  fontSize: 7.w,
                                ),),
                              ),
                              Positioned(
                                  left: 60.w,
                                  top: 7.h,
                                  child: Image(
                                width: 6.w,
                                height: 6.w,
                                image: const AssetImage("android/assets/down-arrow 2.png"),))
                            ],
                          ),
                        ),
                    ) : Center(
                      child: Container(
                        width: 50.w,
                        height: 8.h,
                        margin: EdgeInsetsDirectional.only(start: 20.w),
                        alignment: Alignment.center,
                        child: Row(
                          children: const [
                            CircularProgressIndicator.adaptive(
                              backgroundColor: Color.fromRGBO(
                                  1, 18, 112, 1),
                            ),
                          ],
                        ),
                      ),
                    ),
                ),
                //برقياتي
                Positioned(
                    left: 13.w,
                    top: 50.h,
                    child: myTelLoading == false ? GestureDetector(
                        onTap: () async{
                          setState(() {
                            myTelLoading = true;
                          });
                          await Provider.of<MyService>(context,listen: false).getMyLetters();
                          setState(() {
                            myTelLoading = false;
                          });
                          if(Provider.of<MyService>(context,listen: false).getMyLettersResp!.status == 200) {
                            Navigator.push(context,
                            MaterialPageRoute(builder: (context) => my_telegrams()),
                          );
                          }
                        },
                        child: SizedBox(
                          child: Stack(
                            children: [
                              Image(
                                width: 73.w,
                                height: 35.w,
                                image: const AssetImage("android/assets/Rectangle 72.png"),
                              ),
                              Positioned(
                                left: 25.w,
                                top: 5.h,
                                child: Text("برقياتي",style: TextStyle(
                                  fontFamily: "theFont",
                                  color: const Color.fromRGBO(1, 18, 112, 1),
                                  fontSize: 7.w,
                                ),),
                              ),
                              Positioned(
                                  left: 60.w,
                                  top: 7.h,
                                  child: Image(
                                    width: 6.w,
                                    height: 6.w,
                                    image: AssetImage("android/assets/down-arrow 2.png"),))
                            ],
                          ),
                        ),
                    ) : Center(
                      child: Container(
                        width: 50.w,
                        height: 8.h,
                        margin: EdgeInsetsDirectional.only(start: 20.w),
                        alignment: Alignment.center,
                        child: Row(
                          children: const [
                            CircularProgressIndicator.adaptive(
                              backgroundColor: Color.fromRGBO(
                                  1, 18, 112, 1),
                            ),
                          ],
                        ),
                      ),
                    ),
                ),
                //notifications
                Visibility(
                  visible: _bellVisability,
                  child: Positioned(
                      left: 10.w,
                      top: 8.h,
                      child: notifyLoading == false ? GestureDetector(
                          onTap: () async{
                            setState(() {
                              notifyLoading = true;
                            });
                            await Provider.of<MyService>(context,listen: false).notifications();
                            setState(() {
                              notifyLoading = false;
                            });
                            if(Provider.of<MyService>(context,listen: false).notificationsResp!.status == 200){
                            Navigator.push(context,
                            MaterialPageRoute(builder: (context) => const Notifications()),
                            );
                            }else{
                              Fluttertoast.showToast(
                                  msg: 'لا يوجد اشعارات.',
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 2,
                                  backgroundColor: const Color.fromRGBO(
                                      1, 18, 112, 1),
                                  textColor: Colors.white,
                                  fontSize: 16.0
                              );
                            }
                          },
                          child: Row(
                            children: [
                              Stack(
                                  alignment: AlignmentDirectional.bottomEnd,
                                children: [
                                  Image(
                                  width: 10.w,
                                  height: 10.w,
                                  image: const AssetImage("android/assets/bell.png"),
                                ),
                                  Padding(
                                    padding: const EdgeInsetsDirectional.only(
                                        bottom: 0.0,
                                        end: 0.0
                                    ),
                                    child: CircleAvatar(
                                      radius: 9,
                                      backgroundColor: Colors.red,
                                      child: Padding(
                                        padding: EdgeInsets.only(bottom: 2.0),
                                        child: Center(
                                          child: Text(
                                            Provider.of<MyService>(context,listen: false).unseenNotificationsResp!.count.toString(),
                                            style: const TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ]
                              ),
                            ],
                          )
                      ) : Center(
                        child: Container(
                          width: 50.w,
                          height: 8.h,
                          margin: EdgeInsetsDirectional.only(start: 20.w),
                          alignment: Alignment.center,
                          child: Row(
                            children: const [
                              CircularProgressIndicator.adaptive(
                                backgroundColor: Color.fromRGBO(
                                    1, 18, 112, 1),
                              ),
                            ],
                          ),
                        ),
                      ),
                  ),
                ),
                //open Drawer
                Positioned(
                    left: 80.w,
                    top: 8.h,
                    child: GestureDetector(
                    onTap: (){
                      _scaffoldKey.currentState!.openEndDrawer();
                    },
                    child: Image(
                      width: 10.w,
                      height: 10.w,
                      color: Colors.white,
                      image: const AssetImage("android/assets/menu.png"),
                    ))),
              ],
            ),
          ),
        );
      },
    );
  }
}
