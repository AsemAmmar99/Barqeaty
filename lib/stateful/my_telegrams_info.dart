import 'package:barkeaty/shared/network/local/helpers/cache_helper.dart';
import 'package:barkeaty/shared/network/remote/service/my_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/style.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

class MyTelegramsInfo extends StatefulWidget {
  const MyTelegramsInfo({Key? key}) : super(key: key);

  @override
  _MyTelegramsInfoState createState() => _MyTelegramsInfoState();
}

bool loading = false;

class _MyTelegramsInfoState extends State<MyTelegramsInfo> {
  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
        return Scaffold(
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            child: Column(
              children: [
                Stack(
                  children: [
                    Container(
                      color: const Color.fromRGBO(1, 18, 112, 1),
                      height: 35.h,
                    ),
                    Positioned(
                      top: 4.h,
                      left: 2.w,
                      child: GestureDetector(
                        onTap: (){
                          Navigator.pop(context);
                        },
                        child: Image(
                            width: 15.w,
                            height: 15.w,
                            image: const AssetImage("android/assets/straight-right-arrow.png")),
                      ),
                    ),
                    Positioned(
                      top: 5.h,
                      left: 40.w,
                      child: Text("${Provider.of<MyService>(context,listen: false).getMyLettersInfoResp!.letter.id} :البرقية",style: TextStyle(
                        fontSize: 5.w,
                        fontFamily: "theFont",
                        color: Colors.white,
                      ),
                      ),
                    ),
                    Positioned(
                      top: 12.h,
                      left: 15.w,
                      right: 15.w,
                      child: Container(
                        height: 20.h,
                        width: 100.w,
                        padding: EdgeInsets.only(top: 0.7.h, bottom: 1.h, left: 2.w, right: 2.w),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(2.w),
                        ),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                SizedBox(width: 40.w,),
                                Text(
                                  ':التاريخ والوقت',
                                style: TextStyle(
                                  color: Colors.blueGrey,
                                  fontSize: 3.w,
                                ),),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Expanded(
                                  flex: 3,
                                  child: Text(
                                      Provider.of<MyService>(context,listen: false).getMyLettersInfoResp!.letter.createdAt.toString(),
                                    textAlign: TextAlign.center,
                                    style: const TextStyle(
                                      color: Color.fromRGBO(1, 18, 112, 1),
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                Flexible(
                                  flex: 1,
                                  child: Container(
                                    width: 5.h,
                                    height: 5.h,
                                    decoration: const BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Color.fromRGBO(105, 203, 248, 1),
                                    ),
                                    child: Image(
                                      height: 1.h,
                                        width: 1.h,
                                        image: const AssetImage("android/assets/calendar.png"),),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 1.h,),
                            Container(
                              height: 0.2.w,
                              width: 65.w,
                              color: Colors.black26,
                            ),
                            SizedBox(height: 0.7.h,),
                            Row(
                              children: [
                                SizedBox(width: 37.w,),
                                Text(
                                  ':الجهة المرسل إليها',
                                  style: TextStyle(
                                    color: Colors.blueGrey,
                                    fontSize: 3.w,
                                  ),),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Expanded(
                                  flex: 3,
                                  child: Text(
                                    Provider.of<MyService>(context,listen: false).getMyLettersInfoResp!.letter.service.name,
                                    textAlign: TextAlign.center,
                                    style: const TextStyle(
                                      color: Color.fromRGBO(1, 18, 112, 1),
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                Flexible(
                                  flex: 1,
                                  child: Container(
                                    width: 5.h,
                                    height: 5.h,
                                    decoration: const BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Color.fromRGBO(105, 203, 248, 1),
                                    ),
                                    child: Image(
                                      height: 1.h,
                                      width: 1.h,
                                      image: const AssetImage("android/assets/sent.png"),),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                    ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Text("نص البرقية",style: TextStyle(
                      fontSize: 5.w,
                      fontFamily: "theFont",
                      color: const Color.fromRGBO(1, 18, 112, 1),
                    ),),
                    SizedBox(height: 1.h,),
                    Container(
                      height: 0.2.w,
                      width: 75.w,
                      color: Colors.black26,
                    ),
                    SizedBox(height: 1.h,),
                    Container(
                      width: 70.w,
                      alignment: Alignment.topCenter,
                      child: Text(
                        Provider.of<MyService>(context,listen: false).getMyLettersInfoResp!.letter.letterText,
                        style: TextStyle(
                        fontSize: 3.w,
                        fontFamily: "theFont",
                        color: const Color.fromRGBO(1, 18, 112, 1),
                      ),),
                    ),
                    SizedBox(height: 1.h,),
                    Container(
                      height: 0.2.w,
                      width: 75.w,
                      color: Colors.black26,
                    ),
                    SizedBox(height: 2.h,),
                    Row(
                      children: [
                        SizedBox(width: 10.w,),
                        Column(
                          children: [
                            Container(
                              width: 18.w,
                              height: 18.w,
                              alignment: Alignment.center,
                              padding: EdgeInsets.all(2.w),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                  color: Colors.black45,
                                  width: .5.w,
                                ),
                              ),
                              child: Container(
                                alignment: Alignment.center,
                                child: Text(
                                    CacheHelper.getData(key: 'myTelSts'),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            SizedBox(height: 2.h,),
                            const Text(
                                ' الحالة'
                            ),
                          ],
                        ),
                        SizedBox(width: 13.w,),
                        Column(
                          children: [
                            Container(
                              width: 18.w,
                              height: 18.w,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                  color: const Color.fromRGBO(1, 18, 112, 1),
                                  width: .5.w,
                                ),
                              ),
                              child: Container(
                                alignment: Alignment.center,
                                child: Text(
                                  Provider.of<MyService>(context,listen: false).getMyLettersInfoResp!.letter.attachCount.toString(),
                                  style: TextStyle(
                                    color: const Color.fromRGBO(1, 18, 112, 1),
                                    fontSize: 3.h,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 2.h,),
                            const Text(
                                'المرفقات  ',
                              style: TextStyle(
                                color: Color.fromRGBO(1, 18, 112, 1),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(width: 15.w,),
                        Column(
                          children: [
                            Container(
                              width: 18.w,
                              height: 18.w,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                  color: const Color.fromRGBO(105, 203, 248, 1),
                                  width: .5.w,
                                ),
                              ),
                              child: Container(
                                alignment: Alignment.center,
                                child: Text(
                                  Provider.of<MyService>(context,listen: false).getMyLettersInfoResp!.letter.wordsCount.toString(),
                                  style: TextStyle(
                                    color: const Color.fromRGBO(105, 203, 248, 1),
                                    fontSize: 3.h,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 2.h,),
                            const Text(
                              'عدد الكلمات',
                              style: TextStyle(
                                color: Color.fromRGBO(105, 203, 248, 1),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),

                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 40),
                      child: Column(

                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [

                          Padding(
                            padding: EdgeInsetsDirectional.only(top: 3.h),
                            child: Row(

                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Row(
                                      children: [
                                        const Text('ريال'),
                                        const SizedBox(width: 10,),
                                        Text('${Provider.of<MyService>(context,listen: false).getMyLettersInfoResp!.letter.service.minCost}'),

                                      ],
                                    )
                                  ],
                                ),

                                Text('تكلفة الخدمة',textDirection: TextDirection.rtl,),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsetsDirectional.only(top: 3.h),
                            child: Row(

                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Row(
                                      children: [
                                        const Text('ريال'),
                                        const SizedBox(width: 10,),
                                        Text('${Provider.of<MyService>(context,listen: false).getMyLettersInfoResp!.letter.extraWordsPrice}'),

                                      ],
                                    )
                                  ],
                                ),

                                Text('كلمات زائده (${Provider.of<MyService>(context,listen: false).getMyLettersInfoResp!.letter.extraWordsCount})',textDirection: TextDirection.rtl,),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsetsDirectional.only(top: 3.h),
                            child: Row(

                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Row(
                                      children: [
                                        const Text('ريال'),
                                        const SizedBox(width: 10,),
                                        Text('${Provider.of<MyService>(context,listen: false).getMyLettersInfoResp!.letter.service.attachPrice}'),

                                      ],
                                    )
                                  ],
                                ),

                                Text('مرفقات (${Provider.of<MyService>(context,listen: false).getMyLettersInfoResp!.letter.attachmentsCount})',textDirection: TextDirection.rtl,),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsetsDirectional.only(top: 3.h),
                            child: Row(

                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Row(
                                      children: [
                                        const Text('ريال'),
                                        const SizedBox(width: 10,),
                                        Text('${Provider.of<MyService>(context,listen: false).getMyLettersInfoResp!.letter.discount}'),

                                      ],
                                    )
                                  ],
                                ),

                                Text('خصم',textDirection: TextDirection.rtl,),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsetsDirectional.only(top: 3.h),
                            child: Row(

                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Row(
                                      children: [
                                        const Text('ريال'),
                                        const SizedBox(width: 10,),
                                        Text('${Provider.of<MyService>(context,listen: false).getMyLettersInfoResp!.letter.service.vat}'),

                                      ],
                                    )
                                  ],
                                ),

                               const Text('ضريبة القيمه المضافه',textDirection: TextDirection.rtl,),
                              ],
                            ),
                          ),

                        ],
                      ),
                    ),
                    SizedBox(height: 3.h,),
                    Stack(
                      children: [
                        Container(
                          alignment: Alignment.centerRight,
                          padding: EdgeInsets.all(1.h),
                          height: 6.h,
                          width: 80.w,
                          child: Text("التكلفة الاجمالية",style: TextStyle(
                            fontSize: 5.w,
                            color: const Color.fromRGBO(1, 18, 112, 1),
                          ),),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.35),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset: Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(right: 2.w),
                          height: 6.h,
                          width: 23.w,
                          child: Row(
                            children: [
                              Text("ريال",
                                style: TextStyle(
                                  fontSize: 5.w,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),),
                              const SizedBox(width: 7,),
                              Text(
                                 ' ${Provider.of<MyService>(context,listen: false).getMyLettersInfoResp!.letter.totalCost}',
                                style: TextStyle(
                                fontSize: 5.w,
                                color: Colors.white,
                                  fontWeight: FontWeight.bold,
                              ),),
                            ],
                          ),
                          decoration: BoxDecoration(
                            color: const Color.fromRGBO(105, 203, 248, 1),
                            borderRadius: BorderRadius.circular(20),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.35),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset: Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 3.h,),
                    // loading == false ? TextButton(
                    //     onPressed: () async{
                    //       setState(() {
                    //         loading = true;
                    //       });
                    //       await Provider.of<MyService>(context,listen: false).resendLetter(
                    //           Provider.of<MyService>(context,listen: false).getMyLettersInfoResp!.letter.id
                    //       );
                    //       setState(() {
                    //         loading = false;
                    //       });
                    //       if(Provider.of<MyService>(context,listen: false).resendLetterResp!.status == 200){
                    //         Fluttertoast.showToast(
                    //             msg: 'تم اعادة الارسال بنجاح.',
                    //             toastLength: Toast.LENGTH_SHORT,
                    //             gravity: ToastGravity.CENTER,
                    //             timeInSecForIosWeb: 2,
                    //             backgroundColor: const Color.fromRGBO(
                    //                 1, 18, 112, 1),
                    //             textColor: Colors.white,
                    //             fontSize: 16.0
                    //         );
                    //       }else{
                    //         Fluttertoast.showToast(
                    //             msg: 'فشل اعادة الارسال.',
                    //             toastLength: Toast.LENGTH_SHORT,
                    //             gravity: ToastGravity.CENTER,
                    //             timeInSecForIosWeb: 2,
                    //             backgroundColor: const Color.fromRGBO(
                    //                 1, 18, 112, 1),
                    //             textColor: Colors.white,
                    //             fontSize: 16.0
                    //         );
                    //       }
                    //     },
                    //     child: Text(
                    //       'اعادة الارسال',
                    //       style: TextStyle(
                    //         fontSize: 5.w,
                    //         fontWeight: FontWeight.bold,
                    //       ),
                    //     ),
                    // ) : Center(
                    //   child: Container(
                    //     width: 50.w,
                    //     height: 8.h,
                    //     margin: EdgeInsetsDirectional.only(start: 20.w),
                    //     alignment: Alignment.centerRight,
                    //     child: Row(
                    //       children: const [
                    //         CircularProgressIndicator.adaptive(
                    //           backgroundColor: Color.fromRGBO(
                    //               1, 18, 112, 1),
                    //         ),
                    //       ],
                    //     ),
                    //   ),
                    // ),
                    // SizedBox(height: 3.h,),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
