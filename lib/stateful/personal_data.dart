

import 'package:barkeaty/shared/models/city_model.dart';
import 'package:barkeaty/shared/network/local/helpers/cache_helper.dart';
import 'package:barkeaty/shared/network/remote/responses/get_cities_response.dart';
import 'package:barkeaty/shared/network/remote/service/my_service.dart';
import 'package:barkeaty/stateful/phone_number.dart';
import 'package:barkeaty/stateful/verification_code.dart';
import 'package:barkeaty/stateless/telegram-terms.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';


class personal_data extends StatefulWidget {
  const personal_data({Key? key}) : super(key: key);

  @override
  _personal_dataState createState() => _personal_dataState();
}

class _personal_dataState extends State<personal_data> {

  dynamic dropdownValue;
  dynamic dropdownValue1;
  List citiesList = [];
  var idNumberController = TextEditingController();
  var nameController = TextEditingController();
  var emailController = TextEditingController();
  var phoneController = TextEditingController();
  var formKey = GlobalKey<FormState>();
  bool loading = false;
  bool termsLoading = false;
  bool termsValue = false;

  @override
  Widget build(BuildContext context) {

    List regionsList = Provider.of<MyService>(context,listen: false).regions!.regions;

    return Sizer(
      builder: (context, orientation, deviceType) {
        return Scaffold(
          backgroundColor: Colors.white,
          body: SizedBox(
            height: 100.h,
            child: SingleChildScrollView(
              child: Form(
                key: formKey,
                child: Column(
                  children: [
                    Row(
                      children: [
                        SizedBox(width: 10.w,),
                        GestureDetector(
                          onTap: (){
                            Navigator.pop(context);
                          },
                          child: Image(
                              width: 15.w,
                              height: 15.w,
                              color: Color.fromRGBO(1, 18, 112, 1),
                              image: AssetImage("android/assets/straight-right-arrow.png")),
                        ),
                        SizedBox(width: 10.w,),
                        Image(
                            width: 50.w,
                            height: 20.h,
                            image: const AssetImage("android/assets/barqiaty.png")),
                      ],
                    ),
                    Row(
                      children: [
                        SizedBox(width: 35.w,),
                        Text("بياناتك الشخصية",
                          style: TextStyle(
                            color: const Color.fromRGBO(1, 18, 112, 1),
                            fontFamily: "theFont",
                            fontSize: 9.w,
                          ),),
                      ],
                    ),
                    Row(
                      children: [
                        SizedBox(width: 12.w,),
                        Text("الرجاء ادخال بياناتك بالكامل حتى تتمكن من المتابعه \n                               حيث ان البيانات غير قابلة للتغيير",
                          style: TextStyle(
                            color:  const Color.fromRGBO(112, 116, 162, 1),
                            fontFamily: "theFont",
                            fontSize: 4.w,
                          ),),
                      ],
                    ),
                    SizedBox(height: 4.h,),
                    Row(
                      children: [
                        SizedBox(width: 47.w,),
                        Text("رقم الهوية/الاقامة",
                          style: TextStyle(
                            color:  const Color.fromRGBO(112, 116, 162, 1),
                            fontFamily: "theFont",
                            fontSize: 4.8.w,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 1.h,),
                    Row(
                      children: [
                        SizedBox(width: 15.w,),
                        Container(
                          width: 70.w,
                          decoration: const BoxDecoration(
                            //color: Color.fromRGBO(245, 153, 45, 1),
                            borderRadius: BorderRadius.all(Radius.circular(25.0)),
                          ),
                          child: TextFormField(
                            onChanged: (val){},
                            textAlign: TextAlign.right,
                            keyboardType: TextInputType.number,
                            obscureText: false,
                            obscuringCharacter: "x",
                            enabled: true,
                            controller: idNumberController,
                            validator: (value){
                              if (value == null || value.isEmpty) {
                                return '.أدخل رقم الهوية/الاقامة';
                              }
                            },
                            style: TextStyle(
                              fontFamily: 'theFont',
                              fontSize: 4.w,
                              color: Colors.white,
                            ),
                            cursorColor: const Color.fromRGBO(3, 141, 151, 1),
                            decoration: InputDecoration(
                              suffixIcon: Image(
                                image: const AssetImage("android/assets/driver-license.png"),
                                width: 3.w,
                                height: 3.w,
                              ),
                              contentPadding: EdgeInsets.all(1.w),
                              isDense: true,
                              fillColor: const Color.fromRGBO(1, 18, 112, 1),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              filled: true,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 1.h,),
                    Row(
                      children: [
                        SizedBox(width: 30.w,),
                        Text("الاسم الثلاثي حسب الهوية",
                          style: TextStyle(
                            color:  const Color.fromRGBO(112, 116, 162, 1),
                            fontFamily: "theFont",
                            fontSize: 4.8.w,
                          ),),
                      ],
                    ),
                    SizedBox(height: 1.h,),
                    Row(
                      children: [
                        SizedBox(width: 15.w,),
                        Container(
                          width: 70.w,
                          decoration: const BoxDecoration(
                            //color: Color.fromRGBO(245, 153, 45, 1),
                            borderRadius: BorderRadius.all(Radius.circular(25.0)),
                          ),
                          child: TextFormField(
                            onChanged: (val){},
                            textAlign: TextAlign.right,
                            //keyboardType: TextInputType.number,
                            obscureText: false,
                            //obscuringCharacter: "x",
                            enabled: true,
                            controller: nameController,
                            validator: (value){
                              if (value == null || value.isEmpty) {
                                return '.أدخل اسمك الثلاثي';
                              }
                            },
                            style: TextStyle(
                              fontFamily: 'theFont',
                              fontSize: 4.w,
                              color: Colors.white,
                            ),
                            cursorColor: const Color.fromRGBO(3, 141, 151, 1),
                            decoration: InputDecoration(
                              suffixIcon: Image(
                                image: const AssetImage("android/assets/user.png"),
                                width: 3.w,
                                height: 3.w,
                              ),
                              contentPadding: EdgeInsets.all(1.w),
                              isDense: true,
                              fillColor: const Color.fromRGBO(1, 18, 112, 1),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              filled: true,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 2.h,),
                    Row(
                      children: [
                        SizedBox(width: 50.w,),
                        Text("البريد الالكتروني",
                          style: TextStyle(
                            color:  const Color.fromRGBO(112, 116, 162, 1),
                            fontFamily: "theFont",
                            fontSize: 4.8.w,
                          ),),
                      ],
                    ),
                    SizedBox(height: 1.h,),
                    Row(
                      children: [
                        SizedBox(width: 15.w,),
                        Container(
                          width: 70.w,
                          decoration: const BoxDecoration(
                            //color: Color.fromRGBO(245, 153, 45, 1),
                            borderRadius: BorderRadius.all(Radius.circular(25.0)),
                          ),
                          child: TextFormField(
                            onChanged: (val){},
                            textAlign: TextAlign.right,
                            //keyboardType: TextInputType.number,
                            obscureText: false,
                            //obscuringCharacter: "x",
                            enabled: true,
                            controller: emailController,
                            validator: (value){
                              if (value == null || value.isEmpty) {
                                return '.أدخل بريدك الالكتروني';
                              }
                            },
                            style: TextStyle(
                              fontFamily: 'theFont',
                              fontSize: 4.w,
                              color: Colors.white,
                            ),
                            cursorColor: const Color.fromRGBO(3, 141, 151, 1),
                            decoration: InputDecoration(
                              suffixIcon: Image(
                                image: const AssetImage("android/assets/email.png"),
                                width: 3.w,
                                height: 3.w,
                              ),
                              contentPadding: EdgeInsets.all(1.w),
                              isDense: true,
                              fillColor: const Color.fromRGBO(1, 18, 112, 1),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              filled: true,
                            ),
                          ),
                        ),
                      ],
                    ),SizedBox(height: 2.h,),
                    Row(
                      children: [
                        SizedBox(width: 62.w,),
                        Text("رقم الجوال",
                          style: TextStyle(
                            color:  const Color.fromRGBO(112, 116, 162, 1),
                            fontFamily: "theFont",
                            fontSize: 4.8.w,
                          ),),
                      ],
                    ),
                    SizedBox(height: 1.h,),
                    Row(
                      children: [
                        SizedBox(width: 15.w,),
                        Container(
                          width: 70.w,
                          decoration: const BoxDecoration(
                            //color: Color.fromRGBO(245, 153, 45, 1),
                            borderRadius: BorderRadius.all(Radius.circular(25.0)),
                          ),
                          child: TextFormField(
                            onChanged: (val){},
                            textAlign: TextAlign.right,
                            keyboardType: TextInputType.number,
                            obscureText: false,
                            //obscuringCharacter: "x",
                            enabled: true,
                            controller: phoneController,
                            validator: (value){
                              if (value == null || value.isEmpty) {
                                return '.أدخل رقم جوالك';
                              }
                            },
                            style: TextStyle(
                              fontFamily: 'theFont',
                              fontSize: 4.w,
                              color: Colors.white,
                            ),
                            cursorColor: const Color.fromRGBO(3, 141, 151, 1),
                            decoration: InputDecoration(
                              suffixIcon: Image(
                                image: const AssetImage("android/assets/driver-license.png"),
                                width: 3.w,
                                height: 3.w,
                              ),
                              contentPadding: EdgeInsets.all(1.w),
                              isDense: true,
                              fillColor: const Color.fromRGBO(1, 18, 112, 1),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              filled: true,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 2.h,),
                    Row(
                      children: [
                        SizedBox(width: 68.w,),
                        Text("المنطقة",
                          style: TextStyle(
                            color:  const Color.fromRGBO(112, 116, 162, 1),
                            fontFamily: "theFont",
                            fontSize: 4.8.w,
                          ),),
                      ],
                    ),
                    SizedBox(height: 1.h,),
                    Container(
                      width: 70.w,
                      //alignment: Alignment.centerLeft,
                      padding: EdgeInsets.fromLTRB(2.w, 0.0, 2.w, 0.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(3.w)),
                        border: Border.all(
                          width: .5.w,
                          color: const Color.fromRGBO(112, 116, 162, 1),
                        ),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButtonFormField<String>(
                          decoration: const InputDecoration(
                              enabledBorder: UnderlineInputBorder(borderSide: BorderSide(
                                  color: Colors.white
                              ),
                              ),
                          ),
                          isExpanded: true,
                          icon: Image(image: const AssetImage("android/assets/down-arrow.png"),
                            width: 5.w,height: 5.h,),
                          //borderRadius: BorderRadius.circular(5.w),
                          style: const TextStyle(color: const Color.fromRGBO(112, 116, 162, 1),
                              fontFamily: "theFont"),
                          onChanged: (String? newValue) async{
                            await Provider.of<MyService>(context,listen: false).getCities(newValue);
                            setState((){
                              dropdownValue1 = newValue!;
                              dropdownValue = null;
                              citiesList.clear();
                              citiesList = Provider.of<MyService>(context,listen: false).cities!.cities;
                            });
                          },
                          value: dropdownValue1,
                          hint: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              const Text(
                                '..اختر منطقتك',
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(width: 2.w,),
                            ],
                          ),
                          validator: (value) {
                            if (value == null) {
                              return 'اختر منطقتك..';
                            }
                          },
                          items: regionsList
                              .map((value) {
                            return DropdownMenuItem(
                              value: value.id.toString(),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Container(
                                    alignment: Alignment.centerRight,
                                      child: Text(
                                          value.name,
                                        textAlign: TextAlign.center,
                                      ),
                                  ),
                                  SizedBox(width: 2.w,)
                                ],
                              ),
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                    SizedBox(height: 4.h,),
                    Row(
                      children: [
                        SizedBox(width: 70.w,),
                        Text("المدينة",
                          style: TextStyle(
                            color:  const Color.fromRGBO(112, 116, 162, 1),
                            fontFamily: "theFont",
                            fontSize: 4.8.w,
                          ),),
                      ],
                    ),
                    SizedBox(height: 1.h,),
                    Container(
                      width: 70.w,
                      //alignment: Alignment.centerLeft,
                      padding: EdgeInsets.fromLTRB(2.w, 0.0, 2.w, 0.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(3.w)),
                        border: Border.all(
                          width: .5.w,
                        color: const Color.fromRGBO(112, 116, 162, 1),
                        ),
                      ),
                      child: DropdownButtonHideUnderline(
                              child: DropdownButtonFormField<String>(
                                decoration: const InputDecoration(
                                    enabledBorder: UnderlineInputBorder(borderSide: BorderSide(
                                        color: Colors.white
                                    )
                                    )
                                ),
                                isExpanded: true,
                                icon: Image(image: const AssetImage("android/assets/down-arrow.png"),
                                width: 5.w,height: 5.h,),
                                //borderRadius: BorderRadius.circular(5.w),
                                style: const TextStyle(color: const Color.fromRGBO(112, 116, 162, 1),
                                    fontFamily: "theFont"),
                                onChanged: (String? newValue) {
                                  setState(() {
                                    if(newValue != null) {
                                      dropdownValue = newValue;
                                    }
                                  });
                                },
                                value: dropdownValue,
                                hint: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    const Text(
                                        '..اختر مدينتك',
                                      textAlign: TextAlign.center,
                                    ),
                                    SizedBox(width: 2.w,),
                                  ],
                                ),
                                validator: (value) {
                                  if (value == null) {
                                    return 'اختر مدينتك..';
                                  }
                                },
                                items:
                                    citiesList.map((value) {
                                  return DropdownMenuItem(
                                    value: value.id.toString(),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Container(
                                            alignment: Alignment.centerRight,
                                            child: Text(
                                                value.name ?? 'اختر مدينتك..',
                                              textAlign: TextAlign.center,
                                            ),
                                        ),
                                        SizedBox(width: 2.w,)
                                      ],
                                    ) ,
                                  );
                                }).toList(),
                              ),
                            ),
                    ),
                    SizedBox(height: 5.h,),
                    termsLoading == false ? Row(
                      children: [
                        SizedBox(width: 12.w,),
                        TextButton(onPressed: () async{
                          setState(() {
                            termsLoading = true;
                          });
                          await Provider.of<MyService>(context,listen: false).getLetterTerms();
                          setState(() {
                            termsLoading = false;
                          });
                          if(Provider.of<MyService>(context,listen: false).getLetterTermsResp!.status == 200) {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                              builder: (context) => telegram_terms(),
                          ),
                          );
                          }
                        }, child: Text(
                          'الشروط والأحكام',
                          style: TextStyle(
                            fontFamily: "theFont",
                            fontSize: 4.8.w,
                          ),
                        ),
                        ),
                        SizedBox(width: 2.w,),
                        Text(
                          'أوافق على كافة',
                          style: TextStyle(
                            color:  const Color.fromRGBO(112, 116, 162, 1),
                            fontFamily: "theFont",
                            fontSize: 4.8.w,
                          ),
                        ),
                        SizedBox(width: 5.w,),
                        Checkbox(
                          value: termsValue,
                          onChanged: (value){
                            setState(() {
                              termsValue = value!;
                            });
                          },
                        ),
                      ],
                    )
                        : Center(
                      child: Container(
                        width: 50.w,
                        height: 8.h,
                        margin: EdgeInsetsDirectional.only(start: 20.w),
                        alignment: Alignment.center,
                        child: Row(
                          children: const [
                            CircularProgressIndicator.adaptive(
                              backgroundColor: Color.fromRGBO(
                                  1, 18, 112, 1),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 5.h,),
                    Row(
                      children: [
                        SizedBox(width: 25.w,),
                        GestureDetector(
                          onTap: () async{
                            if(formKey.currentState!.validate()) {
                              if(termsValue == true) {
                                setState(() {
                                  loading = true;
                                });
                                await Provider.of<MyService>(
                                    context, listen: false)
                                    .userRegister(
                                  idNumber: idNumberController.text,
                                  name: nameController.text,
                                  phone: phoneController.text,
                                  districtId: dropdownValue1,
                                  email: emailController.text,
                                );
                                setState(() {
                                  loading = false;
                                });
                                if (Provider
                                    .of<MyService>(context, listen: false)
                                    .register!
                                    .status == 200) {
                                  setState(() {
                                    loading = false;
                                    Fluttertoast.showToast(
                                        msg: Provider
                                            .of<MyService>(
                                            context, listen: false)
                                            .register!
                                            .message,
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.CENTER,
                                        timeInSecForIosWeb: 2,
                                        backgroundColor: const Color.fromRGBO(
                                            1, 18, 112, 1),
                                        textColor: Colors.white,
                                        fontSize: 16.0
                                    );
                                  });
                                  Navigator.pushAndRemoveUntil(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => code(),
                                    ),
                                        (route) {
                                      return false;
                                    },
                                  );
                                }
                              }else{
                                Fluttertoast.showToast(
                                    msg: 'يجب عليك الموافقة على الشروط والأحكام أولا',
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 2,
                                    backgroundColor: const Color.fromRGBO(
                                        1, 18, 112, 1),
                                    textColor: Colors.white,
                                    fontSize: 16.0
                                );
                              }
                            }
                          },
                          child: loading == false ? Container(
                            width: 50.w,
                            height: 8.h,
                            alignment: Alignment.center,
                            decoration: const BoxDecoration(
                              color: Color.fromRGBO(105, 203, 248, 1),
                              borderRadius: BorderRadius.all(Radius.circular(50.0)),
                            ),
                            child: Text("حفظ بياناتي",style: TextStyle(
                              fontSize: 6.5.w,
                              color: Colors.white,
                              fontFamily: "theFont",
                            ),
                            ),
                          ) : Center(
                            child: Container(
                              width: 50.w,
                              height: 8.h,
                              margin: EdgeInsetsDirectional.only(start: 20.w),
                              alignment: Alignment.center,
                              child: Row(
                                children: const [
                                  CircularProgressIndicator.adaptive(
                                    backgroundColor: Color.fromRGBO(
                                        1, 18, 112, 1),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 5.h,),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
