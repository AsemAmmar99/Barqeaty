import 'dart:io';

import 'package:barkeaty/shared/models/attachment_model.dart';
import 'package:barkeaty/shared/network/local/helpers/cache_helper.dart';
import 'package:barkeaty/shared/network/remote/helpers/dio_helper.dart';
import 'package:barkeaty/shared/network/remote/responses/check_coupon_response.dart';
import 'package:barkeaty/shared/network/remote/responses/get_about_us_response.dart';
import 'package:barkeaty/shared/network/remote/responses/get_cities_response.dart';
import 'package:barkeaty/shared/network/remote/responses/get_directions_response.dart';
import 'package:barkeaty/shared/network/remote/responses/get_letter_before_start_response.dart';
import 'package:barkeaty/shared/network/remote/responses/get_letter_terms_response.dart';
import 'package:barkeaty/shared/network/remote/responses/get_my_letters_response.dart';
import 'package:barkeaty/shared/network/remote/responses/get_private_companies_response.dart';
import 'package:barkeaty/shared/network/remote/responses/get_regions_response.dart';
import 'package:barkeaty/shared/network/remote/responses/get_services_response.dart';
import 'package:barkeaty/shared/network/remote/responses/get_templates_response.dart';
import 'package:barkeaty/shared/network/remote/responses/login_response.dart';
import 'package:barkeaty/shared/network/remote/responses/logout_response.dart';
import 'package:barkeaty/shared/network/remote/responses/notifications_response.dart';
import 'package:barkeaty/shared/network/remote/responses/register_response.dart';
import 'package:barkeaty/shared/network/remote/responses/resend_code_response.dart';
import 'package:barkeaty/shared/network/remote/responses/send_letter_response.dart';
import 'package:barkeaty/shared/network/remote/responses/un_seen_notifications_response.dart';
import 'package:barkeaty/shared/network/remote/responses/validate_code_response.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:provider/provider.dart';

class MyService extends ChangeNotifier {

   LoginResponse? login;
   LogoutResponse? logoutResp;
   RegisterResponse? register;
   GetRegionsResponse? regions;
   GetCitiesResponse? cities;
   ValidateCodeResponse? validateCodeResp;
   ResendCodeResponse? resendCodeResp;
   GetDirectionsResponse? getDirectionsResp;
   GetPrivateCompaniesResponse? getPrivateCompaniesResp;
   UnSeenNotificationsResponse? unseenNotificationsResp;
   NotificationsResponse? notificationsResp;
   GetTemplatesResponse? getTemplatesResp;
   GetLetterBeforeStartResponse? getLetterBeforeStartResp;
   GetServicesResponse? getServicesGovResp;
   GetServicesResponse? getServicesCompResp;
   SendLetterResponse? sendLetterResp;
   SendLetterResponse? resendLetterResp;
   CheckCouponResponse? checkCouponResp;
   GetMyLettersResponse? getMyLettersResp;
   GetAboutUsResponse? getAboutUsResp;
   GetLetterTermsResponse? getLetterTermsResp;
   GetLetterTermsResponse? getReturnResp;
   SendLetterResponse? getMyLettersInfoResp;
   dynamic v;


  Future userLogin({
    @required String? idNumber,
  })async {
  await  DioHelper.postData(
      url: 'login',
      data: {
        'idNumber': idNumber,
      },
    ).then((value) {
      print(value.data);
      login = LoginResponse.fromJson(value.data);

      CacheHelper.saveData(key: 'phone', value: login!.phone);

      v = value.statusCode;
      notifyListeners();
      print(login!.message);
    }).catchError((error) {
      print('response+$login');
      print('response+$v');
      print(error.toString());
    });
  }

  Future userRegister({
   String? idNumber,
   String? name,
   String? email,
   String? phone,
   String? districtId,
  })async {
    await  DioHelper.postData(
      url: 'register',
      data: {
        'idNumber': idNumber,
        'name': name,
        'email': email,
        'phone': phone,
        'lang': 'ar',
        'password': '666555453',
        'districtId': districtId,
      },
    ).then((value) {
      print(value.data);
      Fluttertoast.showToast(
          msg: value.data['message'],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 2,
          backgroundColor: const Color.fromRGBO(
              1, 18, 112, 1),
          textColor: Colors.white,
          fontSize: 16.0
      );
      register = RegisterResponse.fromJson(value.data);

      CacheHelper.saveData(key: 'phone', value: register!.account.phone);


      v = value.statusCode;
      notifyListeners();
      print(register!.message);
    }).catchError((error) {
      print('response+$register');
      print('response+$v');
      print(error.toString());
    });
  }

   Future getRegions()async {
     await  DioHelper.postData(
       url: 'getRegions',
       data: {
         'page': 0,
       },
     ).then((value) {
       print(value.data);
       regions = GetRegionsResponse.fromJson(value.data);

       v = value.statusCode;
       notifyListeners();
       print(regions!.status);
     }).catchError((error) {
       print('response+$regions');
       print('response+$v');
       print(error.toString());
     });
   }

   Future getCities(
       String? regionId,
       )async {
     await  DioHelper.postData(
       url: 'getCities',
       data: {
         'regionId': regionId,
       },
     ).then((value) {
       print(value.data);
       cities = GetCitiesResponse.fromJson(value.data);
       notifyListeners();
       v = value.statusCode;
       print(cities!.status);
     }).catchError((error) {
       print('response+$cities');
       print('response+$v');
       print(error.toString());
     });
   }

   Future validateCode(
       String? code,
       )async {
     await  DioHelper.postData(
       url: 'validateCode',
       data: {
         'code': code,
         'phone': CacheHelper.getData(key: 'phone'),
       },
     ).then((value) {
       print(value.data);
       validateCodeResp = ValidateCodeResponse.fromJson(value.data);

       CacheHelper.saveData(key: 'token', value: validateCodeResp!.account.apiToken);
       CacheHelper.saveData(key: 'idNumber', value: validateCodeResp!.account.idNumber);
       CacheHelper.saveData(key: 'name', value: validateCodeResp!.account.name);
       CacheHelper.saveData(key: 'email', value: validateCodeResp!.account.email);
       CacheHelper.saveData(key: 'phone', value: validateCodeResp!.account.phone);
       CacheHelper.saveData(key: 'districtId', value: validateCodeResp!.account.district.id);

       notifyListeners();
       v = value.statusCode;
       print(validateCodeResp!.status);
     }).catchError((error) {
       Fluttertoast.showToast(
           msg: validateCodeResp.toString() == 'null' ? 'رمز التحقق غير صحيح.' : validateCodeResp.toString(),
           toastLength: Toast.LENGTH_SHORT,
           gravity: ToastGravity.CENTER,
           timeInSecForIosWeb: 2,
           backgroundColor: const Color.fromRGBO(
               1, 18, 112, 1),
           textColor: Colors.white,
           fontSize: 16.0
       );
       print('response+$validateCodeResp');
       print('response+$v');
       print(error.toString());
     });
   }

   Future resendCode(
       )async {
     await  DioHelper.postData(
       url: 'resendCode',
       data: {
         'phone': CacheHelper.getData(key: 'phone'),
       },
     ).then((value) {
       print(value.data);
       resendCodeResp = ResendCodeResponse.fromJson(value.data);

       notifyListeners();
       v = value.statusCode;
       print(resendCodeResp!.status);
     }).catchError((error) {
       print('response+$resendCodeResp');
       print('response+$v');
       print(error.toString());
     });
   }

   Future logout()async {
     await  DioHelper.postData(
       url: 'logout',
       data: {
         'apiToken': CacheHelper.getData(key: 'token'),
       },
     ).then((value) {
       print(value.data);
       logoutResp = LogoutResponse.fromJson(value.data);
       notifyListeners();
       v = value.statusCode;
       print(logoutResp!.status);
     }).catchError((error) {
       print('response+$logoutResp');
       print('response+$v');
       print(error.toString());
     });
   }

   Future getDirections()async {
     await  DioHelper.postData(
       url: 'getDirections',
       data: {
         'page': 0,
         'apiToken': CacheHelper.getData(key: 'token'),
       },
     ).then((value) {
       print(value.data);
       getDirectionsResp = GetDirectionsResponse.fromJson(value.data);
       notifyListeners();
       v = value.statusCode;
       print(getDirectionsResp!.status);
     }).catchError((error) {
       print('response+$getDirectionsResp');
       print('response+$v');
       print(error.toString());
     });
   }

   Future getPrivateCompanies()async {
     await  DioHelper.postData(
       url: 'getPrivateCompanies',
       data: {
         'page': 0,
         'lang': 'ar',
       },
     ).then((value) {
       print(value.data);
       getPrivateCompaniesResp = GetPrivateCompaniesResponse.fromJson(value.data);
       notifyListeners();
       v = value.statusCode;
       print(getPrivateCompaniesResp!.status);
     }).catchError((error) {
       print('response+$getPrivateCompaniesResp');
       print('response+$v');
       print(error.toString());
     });
   }

   Future unseenNotifications()async {
     await  DioHelper.postData(
       url: 'unseenNotifications',
       data: {
         'apiToken': CacheHelper.getData(key: 'token'),
       },
     ).then((value) {
       print(value.data);
       unseenNotificationsResp = UnSeenNotificationsResponse.fromJson(value.data);
       notifyListeners();
       v = value.statusCode;
       print(unseenNotificationsResp!.status);
     }).catchError((error) {
       print('response+$unseenNotificationsResp');
       print('response+$v');
       print(error.toString());
     });
   }

   Future notifications()async {
     await  DioHelper.postData(
       url: 'notifications',
       data: {
         'apiToken': CacheHelper.getData(key: 'token'),
         'page': 0,
       },
     ).then((value) {
       print(value.data);
       notificationsResp = NotificationsResponse.fromJson(value.data);
       notifyListeners();
       v = value.statusCode;
       print(notificationsResp!.status);
     }).catchError((error) {
       Fluttertoast.showToast(
           msg: notificationsResp.toString() == 'null' ? 'لا توجد اشعارات.' : notificationsResp.toString(),
           toastLength: Toast.LENGTH_SHORT,
           gravity: ToastGravity.CENTER,
           timeInSecForIosWeb: 2,
           backgroundColor: const Color.fromRGBO(
               1, 18, 112, 1),
           textColor: Colors.white,
           fontSize: 16.0
       );
       print('response+$notificationsResp');
       print('response+$v');
       print(error.toString());
     });
   }

   Future getTemplates()async {
     await  DioHelper.postData(
       url: 'getTemplates',
       data: {
         'directionType': 1,
         'page': 0,
       },
     ).then((value) {
       print(value.data);
       getTemplatesResp = GetTemplatesResponse.fromJson(value.data);
       notifyListeners();
       v = value.statusCode;
       print(getTemplatesResp!.status);
     }).catchError((error) {
       print('response+$getTemplatesResp');
       print('response+$v');
       print(error.toString());
     });
   }

   Future getLetterBeforeStart()async {
     await  DioHelper.postData(
       url: 'getLetterBeforeStart',
       data: {},
     ).then((value) {
       print(value.data);
       getLetterBeforeStartResp = GetLetterBeforeStartResponse.fromJson(value.data);
       notifyListeners();
       v = value.statusCode;
       print(getLetterBeforeStartResp!.status);
     }).catchError((error) {
       print('response+$getLetterBeforeStartResp');
       print('response+$v');
       print(error.toString());
     });
   }

   Future getServicesGov()async {
     await  DioHelper.postData(
       url: 'getServices',
       data: {
         'directionType': 1,
         'page': 0,
       },
     ).then((value) {
       print(value.data);
       getServicesGovResp = GetServicesResponse.fromJson(value.data);
       notifyListeners();
       v = value.statusCode;
       print(getServicesGovResp!.status);
     }).catchError((error) {
       print('response+$getServicesGovResp');
       print('response+$v');
       print(error.toString());
     });
   }

   Future getServicesComp()async {
     await  DioHelper.postData(
       url: 'getServices',
       data: {
         'directionType': 3,
         'page': 0,
       },
     ).then((value) {
       print(value.data);
       getServicesCompResp = GetServicesResponse.fromJson(value.data);
       notifyListeners();
       v = value.statusCode;
       print(getServicesCompResp!.status);
     }).catchError((error) {
       print('response+$getServicesCompResp');
       print('response+$v');
       print(error.toString());
     });
   }

   Future sendLetter(
       List? files
       )async {
     await  DioHelper.postMultipartData(
       url: 'sendLetter',
       formData: FormData.fromMap({
         'attachments': [files],
         'districtId': CacheHelper.getData(key: 'districtId'),
         'apiToken': CacheHelper.getData(key: 'token'),
         'serviceId': CacheHelper.getData(key: 'serviceid'),
         'letterTypeId': 1,
         'letterText': CacheHelper.getData(key: 'letterText'),
         'attachCount': CacheHelper.getData(key: 'attsCount'),
         if (CacheHelper.getData(key: 'privateCompanyId') != null) 'privateCompanyId': CacheHelper.getData(key: 'privateCompanyId'),
         if (CacheHelper.getData(key: 'directionId') != null) 'directionId': CacheHelper.getData(key: 'directionId'),
       }),
     ).then((value) {
       print(value.data);
       sendLetterResp = SendLetterResponse.fromJson(value.data);
       CacheHelper.saveData(key: 'letterId', value: sendLetterResp!.letter.id);
       notifyListeners();
       v = value.statusCode;
       print(sendLetterResp!.status);
     }).catchError((error) {
       print('response+$sendLetterResp');
       print('response+$v');
       print(error.toString());
     });
   }

   Future editLetter(
       List? files
       )async {
     await  DioHelper.postMultipartData(
       url: 'editLetter',
       formData: FormData.fromMap({
         'letterId': CacheHelper.getData(key: 'letterId'),
         'districtId': CacheHelper.getData(key: 'districtId'),
         'attachments': [files],
         'apiToken': CacheHelper.getData(key: 'token'),
         'serviceId': CacheHelper.getData(key: 'serviceid'),
         'letterTypeId': 1,
         'letterText': CacheHelper.getData(key: 'letterText'),
         'attachCount': CacheHelper.getData(key: 'attsCount'),
         if (CacheHelper.getData(key: 'privateCompanyId') != null) 'privateCompanyId': CacheHelper.getData(key: 'privateCompanyId'),
         if (CacheHelper.getData(key: 'directionId') != null) 'directionId': CacheHelper.getData(key: 'directionId'),
       }),
     ).then((value) {
       print(value.data);
       sendLetterResp = SendLetterResponse.fromJson(value.data);
       notifyListeners();
       v = value.statusCode;
       print(sendLetterResp!.status);
     }).catchError((error) {
       print('response+$sendLetterResp');
       print('response+$v');
       print(error.toString());
     });
   }

   Future resendLetter(
       int? letterId,
       )async {
     await  DioHelper.postData(
       url: 'resendLetter',
       data: {
         'letterId': letterId,
         'apiToken': CacheHelper.getData(key: 'token'),
       },
     ).then((value) {
       print(value.data);
       resendLetterResp = SendLetterResponse.fromJson(value.data);
       notifyListeners();
       v = value.statusCode;
       print(resendLetterResp!.status);
     }).catchError((error) {
       print('response+$resendLetterResp');
       print('response+$v');
       print(error.toString());
     });
   }

   Future checkCoupon(
       String? code
       )async {
     await  DioHelper.postData(
       url: 'checkCoupon',
       data: {
         'code': code,
       },
     ).then((value) {
       print(value.data);
       checkCouponResp = CheckCouponResponse.fromJson(value.data);
       notifyListeners();
       v = value.statusCode;
       print(checkCouponResp!.status);
     }).catchError((error) {
       Fluttertoast.showToast(
           msg: 'كود الخصم غير موجود.',
           toastLength: Toast.LENGTH_SHORT,
           gravity: ToastGravity.CENTER,
           timeInSecForIosWeb: 2,
           backgroundColor: const Color.fromRGBO(
               1, 18, 112, 1),
           textColor: Colors.white,
           fontSize: 16.0
       );
       print('response+$checkCouponResp');
       print('response+$v');
       print(error.toString());
     });
   }

   Future getMyLetters()async {
     await  DioHelper.postData(
       url: 'getMyLetters',
       data: {
         'page': 0,
         'apiToken': CacheHelper.getData(key: 'token'),
       },
     ).then((value) {
       print(value.data);
       getMyLettersResp = GetMyLettersResponse.fromJson(value.data);
       notifyListeners();
       v = value.statusCode;
       print(getMyLettersResp!.status);
     }).catchError((error) {
       print('response+$getMyLettersResp');
       print('response+$v');
       print(error.toString());
     });
   }

   Future getMyLettersInfo(
       int? letterId,
       )async {
     await  DioHelper.postData(
       url: 'getMyLettersInfo',
       data: {
         'letterId': letterId,
         'apiToken': CacheHelper.getData(key: 'token'),
       },
     ).then((value) {
       print(value.data);
       getMyLettersInfoResp = SendLetterResponse.fromJson(value.data);
       notifyListeners();
       v = value.statusCode;
       print(getMyLettersInfoResp!.status);
     }).catchError((error) {
       print('response+$getMyLettersInfoResp');
       print('response+$v');
       print(error.toString());
     });
   }

   Future getAboutUs()async {
     await  DioHelper.postData(
       url: 'getAboutUs',
       data: {},
     ).then((value) {
       print(value.data);
       getAboutUsResp = GetAboutUsResponse.fromJson(value.data);
       notifyListeners();
       v = value.statusCode;
       print(getAboutUsResp!.status);
     }).catchError((error) {
       print('response+$getAboutUsResp');
       print('response+$v');
       print(error.toString());
     });
   }

   Future getLetterTerms()async {
     await  DioHelper.postData(
       url: 'getLetterTerms',
       data: {},
     ).then((value) {
       print(value.data);
       getLetterTermsResp = GetLetterTermsResponse.fromJson(value.data);
       notifyListeners();
       v = value.statusCode;
       print(getLetterTermsResp!.status);
     }).catchError((error) {
       print('response+$getLetterTermsResp');
       print('response+$v');
       print(error.toString());
     });
   }

  Future getReturn()async {
       await  DioHelper.postData(
         url: 'getReturn',
         data: {},
       ).then((value) {
         print(value.data);
         getReturnResp = GetLetterTermsResponse.fromJson(value.data);
         notifyListeners();
         v = value.statusCode;
         print(getReturnResp!.status);
       }).catchError((error) {
         print('response+$getReturnResp');
         print('response+$v');
         print(error.toString());
       });
     }

}