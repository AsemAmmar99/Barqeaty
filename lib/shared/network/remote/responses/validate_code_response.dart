import 'dart:convert';

import 'package:barkeaty/shared/models/account_model.dart';

ValidateCodeResponse validateCodeResponseFromJson(String str) => ValidateCodeResponse.fromJson(json.decode(str));

String validateCodeResponseToJson(ValidateCodeResponse data) => json.encode(data.toJson());

class ValidateCodeResponse {
  ValidateCodeResponse({
    required this.status,
    required this.account,
    required this.message,
    required this.apiToken,
  });

  int status;
  Account account;
  String message;
  String apiToken;

  factory ValidateCodeResponse.fromJson(Map<String, dynamic> json) => ValidateCodeResponse(
    status: json["status"],
    account: Account.fromJson(json["account"]),
    message: json["message"],
    apiToken: json["apiToken"] ?? 'noting',
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "account": account.toJson(),
    "message": message,
    "apiToken": apiToken,
  };
}