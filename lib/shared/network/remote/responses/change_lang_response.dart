import 'dart:convert';

ChangeLangResponse changeLangResponseFromJson(String str) => ChangeLangResponse.fromJson(json.decode(str));

String changeLangResponseToJson(ChangeLangResponse data) => json.encode(data.toJson());

class ChangeLangResponse {
  ChangeLangResponse({
    required this.status,
  });

  int status;

  factory ChangeLangResponse.fromJson(Map<String, dynamic> json) => ChangeLangResponse(
    status: json["status"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
  };
}