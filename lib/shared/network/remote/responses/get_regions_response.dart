import 'dart:convert';

import 'package:barkeaty/shared/models/region_model.dart';

GetRegionsResponse getRegionsResponseFromJson(String str) => GetRegionsResponse.fromJson(json.decode(str));

String getRegionsResponseToJson(GetRegionsResponse data) => json.encode(data.toJson());

class GetRegionsResponse {
  GetRegionsResponse({
    required this.status,
    required this.regions,
  });

  int status;
  List<Region> regions;

  factory GetRegionsResponse.fromJson(Map<String, dynamic> json) => GetRegionsResponse(
    status: json["status"],
    regions: List<Region>.from(json["regions"].map((x) => Region.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "regions": List<dynamic>.from(regions.map((x) => x.toJson())),
  };
}