import 'dart:convert';

import 'package:barkeaty/shared/models/coupon_model.dart';

CheckCouponResponse checkCouponResponseFromJson(String str) => CheckCouponResponse.fromJson(json.decode(str));

String checkCouponResponseToJson(CheckCouponResponse data) => json.encode(data.toJson());

class CheckCouponResponse {
  CheckCouponResponse({
    required this.status,
    required this.message,
    required this.coupon,
  });

  int status;
  String message;
  Coupon coupon;

  factory CheckCouponResponse.fromJson(Map<String, dynamic> json) => CheckCouponResponse(
    status: json["status"] ?? 0,
    message: json["message"] ?? 'تم تفعيل كود الخصم.',
    coupon: Coupon.fromJson(json["coupon"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "coupon": coupon.toJson(),
  };
}