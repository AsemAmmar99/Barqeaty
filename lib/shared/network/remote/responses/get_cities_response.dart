import 'dart:convert';

import 'package:barkeaty/shared/models/city_model.dart';

GetCitiesResponse getCitiesResponseFromJson(String str) => GetCitiesResponse.fromJson(json.decode(str));

String getCitiesResponseToJson(GetCitiesResponse data) => json.encode(data.toJson());

class GetCitiesResponse {
  GetCitiesResponse({
    required this.status,
    required this.cities,
  });

  int status;
  List<City> cities;

  factory GetCitiesResponse.fromJson(Map<String, dynamic> json) => GetCitiesResponse(
    status: json["status"],
    cities: List<City>.from(json["cities"].map((x) => City.fromJson(x)) ?? []
    ),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "cities": List<dynamic>.from(cities.map((x) => x.toJson())),
  };
}