import 'dart:convert';

import 'package:barkeaty/shared/models/info_model.dart';

GetInfoResponse getInfoResponseFromJson(String str) => GetInfoResponse.fromJson(json.decode(str));

String getInfoResponseToJson(GetInfoResponse data) => json.encode(data.toJson());

class GetInfoResponse {
  GetInfoResponse({
    required this.status,
    required this.info,
  });

  int status;
  Info info;

  factory GetInfoResponse.fromJson(Map<String, dynamic> json) => GetInfoResponse(
    status: json["status"],
    info: Info.fromJson(json["info"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "info": info.toJson(),
  };
}