import 'dart:convert';

import 'package:barkeaty/shared/models/letter_type_model.dart';

GetLetterTypesResponse getLetterTypesResponseFromJson(String str) => GetLetterTypesResponse.fromJson(json.decode(str));

String getLetterTypesResponseToJson(GetLetterTypesResponse data) => json.encode(data.toJson());

class GetLetterTypesResponse {
  GetLetterTypesResponse({
    required this.status,
    required this.totalPages,
    required this.letterTypes,
  });

  int status;
  int totalPages;
  List<LetterType> letterTypes;

  factory GetLetterTypesResponse.fromJson(Map<String, dynamic> json) => GetLetterTypesResponse(
    status: json["status"],
    totalPages: json["totalPages"],
    letterTypes: List<LetterType>.from(json["letterTypes"].map((x) => LetterType.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "totalPages": totalPages,
    "letterTypes": List<dynamic>.from(letterTypes.map((x) => x.toJson())),
  };
}