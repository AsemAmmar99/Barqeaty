import 'dart:convert';

UnSeenNotificationsResponse unSeenNotificationsResponseFromJson(String str) => UnSeenNotificationsResponse.fromJson(json.decode(str));

String unSeenNotificationsResponseToJson(UnSeenNotificationsResponse data) => json.encode(data.toJson());

class UnSeenNotificationsResponse {
  UnSeenNotificationsResponse({
    required this.status,
    required this.count,
    required this.message,
    required this.account,
  });

  int status;
  int count;
  String message;
  List<dynamic> account;

  factory UnSeenNotificationsResponse.fromJson(Map<String, dynamic> json) => UnSeenNotificationsResponse(
    status: json["status"],
    count: json["count"] ?? 0,
    message: json["message"],
    account: json["account"] == null ? List<dynamic>.from([].map((x) => x)) : List<dynamic>.from(json["account"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "count": count,
    "message": message,
    "account": List<dynamic>.from(account.map((x) => x)),
  };
}