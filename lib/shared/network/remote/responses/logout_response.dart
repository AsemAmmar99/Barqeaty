import 'dart:convert';

LogoutResponse logoutResponseFromJson(String str) => LogoutResponse.fromJson(json.decode(str));

String logoutResponseToJson(LogoutResponse data) => json.encode(data.toJson());

class LogoutResponse {
  LogoutResponse({
    required this.status,
    required this.message,
    required this.account,
  });

  int status;
  String message;
  List<dynamic> account;

  factory LogoutResponse.fromJson(Map<String, dynamic> json) => LogoutResponse(
    status: json["status"],
    message: json["message"] ?? 'تم تسجيل الخروج بنجاح.',
    account: json["account"] == null ? List<dynamic>.from([].map((x) => x)) : List<dynamic>.from(json["account"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "account": List<dynamic>.from(account.map((x) => x)),
  };
}
