import 'dart:convert';

ResendCodeResponse resendCodeResponseFromJson(String str) => ResendCodeResponse.fromJson(json.decode(str));

String resendCodeResponseToJson(ResendCodeResponse data) => json.encode(data.toJson());

class ResendCodeResponse {
  ResendCodeResponse({
    required this.status,
    required this.message,
  });

  int status;
  String message;

  factory ResendCodeResponse.fromJson(Map<String, dynamic> json) => ResendCodeResponse(
    status: json["status"],
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
  };
}