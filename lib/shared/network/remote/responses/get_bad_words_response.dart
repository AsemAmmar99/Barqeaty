import 'dart:convert';

GetBadWordsResponse getBadWordsResponseFromJson(String str) => GetBadWordsResponse.fromJson(json.decode(str));

String getBadWordsResponseToJson(GetBadWordsResponse data) => json.encode(data.toJson());

class GetBadWordsResponse {
  GetBadWordsResponse({
    required this.status,
    required this.badWords,
  });

  int status;
  List<String> badWords;

  factory GetBadWordsResponse.fromJson(Map<String, dynamic> json) => GetBadWordsResponse(
    status: json["status"],
    badWords: List<String>.from(json["badWords"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "badWords": List<dynamic>.from(badWords.map((x) => x)),
  };
}