import 'dart:convert';

import 'package:barkeaty/shared/models/about_us_data_model.dart';

GetAboutUsResponse getAboutUsResponseFromJson(String str) => GetAboutUsResponse.fromJson(json.decode(str));

String getAboutUsResponseToJson(GetAboutUsResponse data) => json.encode(data.toJson());

class GetAboutUsResponse {
  GetAboutUsResponse({
    required this.status,
    required this.aboutUsData,
  });

  int status;
  AboutUsData aboutUsData;

  factory GetAboutUsResponse.fromJson(Map<String, dynamic> json) => GetAboutUsResponse(
    status: json["status"],
    aboutUsData: AboutUsData.fromJson(json["aboutUsData"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "aboutUsData": aboutUsData.toJson(),
  };
}