import 'dart:convert';

import 'package:barkeaty/shared/models/letter_model.dart';

SendLetterResponse sendLetterResponseFromJson(String str) => SendLetterResponse.fromJson(json.decode(str));

String sendLetterResponseToJson(SendLetterResponse data) => json.encode(data.toJson());

class SendLetterResponse {
  SendLetterResponse({
    required this.status,
    required this.message,
    required this.letter,
  });

  int status;
  String message;
  Letter letter;

  factory SendLetterResponse.fromJson(Map<String, dynamic> json) => SendLetterResponse(
    status: json["status"],
    message: json["message"] ?? 'تم حفظ برقيتك.',
    letter: Letter.fromJson(json["letter"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "letter": letter.toJson(),
  };
}