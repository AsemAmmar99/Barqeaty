import 'dart:convert';

UpdateFireBaseTokenResponse updateFireBaseTokenResponseFromJson(String str) => UpdateFireBaseTokenResponse.fromJson(json.decode(str));

String updateFireBaseTokenResponseToJson(UpdateFireBaseTokenResponse data) => json.encode(data.toJson());

class UpdateFireBaseTokenResponse {
  UpdateFireBaseTokenResponse({
    required this.status,
    required this.message,
  });

  int status;
  String message;

  factory UpdateFireBaseTokenResponse.fromJson(Map<String, dynamic> json) => UpdateFireBaseTokenResponse(
    status: json["status"],
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
  };
}