import 'dart:convert';

import 'package:barkeaty/shared/models/notification_model.dart';

NotificationsResponse notificationsResponseFromJson(String str) => NotificationsResponse.fromJson(json.decode(str));

String notificationsResponseToJson(NotificationsResponse data) => json.encode(data.toJson());

class NotificationsResponse {
  NotificationsResponse({
    required this.status,
    required this.totalPages,
    required this.notifications,
  });

  int status;
  int totalPages;
  List<Notification> notifications;

  factory NotificationsResponse.fromJson(Map<String, dynamic> json) => NotificationsResponse(
    status: json["status"],
    totalPages: json["totalPages"],
    notifications: List<Notification>.from(json["notifications"].map((x) => Notification.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "totalPages": totalPages,
    "notifications": List<dynamic>.from(notifications.map((x) => x.toJson())),
  };
}