import 'dart:convert';

LoginResponse loginResponseFromJson(String str) => LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
  LoginResponse({
    required this.status,
    required this.message,
    required this.phone,
  });

  int status;
  String message;
  String phone;

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
    status: json["status"],
    message: json["message"],
    phone: json["phone"] ?? 'failed',
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "phone": phone,
  };
}