import 'dart:convert';

import 'package:barkeaty/shared/models/template_model.dart';

GetTemplatesResponse getTemplatesResponseFromJson(String str) => GetTemplatesResponse.fromJson(json.decode(str));

String getTemplatesResponseToJson(GetTemplatesResponse data) => json.encode(data.toJson());

class GetTemplatesResponse {
  GetTemplatesResponse({
    required this.status,
    required this.totalPages,
    required this.templates,
  });

  int status;
  int totalPages;
  List<Template> templates;

  factory GetTemplatesResponse.fromJson(Map<String, dynamic> json) => GetTemplatesResponse(
    status: json["status"],
    totalPages: json["totalPages"],
    templates: List<Template>.from(json["templates"].map((x) => Template.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "totalPages": totalPages,
    "templates": List<dynamic>.from(templates.map((x) => x.toJson())),
  };
}