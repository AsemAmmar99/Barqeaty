import 'dart:convert';

import 'package:barkeaty/shared/models/account_model.dart';

RegisterResponse registerResponseFromJson(String str) => RegisterResponse.fromJson(json.decode(str));

String registerResponseToJson(RegisterResponse data) => json.encode(data.toJson());

class RegisterResponse {
  RegisterResponse({
    required this.status,
    required this.account,
    required this.message,
  });

  int status;
  Account account;
  String message;

  factory RegisterResponse.fromJson(Map<String, dynamic> json) => RegisterResponse(
    status: json["status"],
    account: Account.fromJson(json["account"] ?? {}),
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "account": account.toJson(),
    "message": message,
  };
}