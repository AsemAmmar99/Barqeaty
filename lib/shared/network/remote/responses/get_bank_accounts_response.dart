import 'dart:convert';

import 'package:barkeaty/shared/models/bank_account_model.dart';

GetBankAccountsResponse getBankAccountsResponseFromJson(String str) => GetBankAccountsResponse.fromJson(json.decode(str));

String getBankAccountsResponseToJson(GetBankAccountsResponse data) => json.encode(data.toJson());

class GetBankAccountsResponse {
  GetBankAccountsResponse({
    required this.status,
    required this.totalPages,
    required this.bankAccounts,
  });

  int status;
  int totalPages;
  List<BankAccount> bankAccounts;

  factory GetBankAccountsResponse.fromJson(Map<String, dynamic> json) => GetBankAccountsResponse(
    status: json["status"],
    totalPages: json["totalPages"],
    bankAccounts: List<BankAccount>.from(json["bankAccounts"].map((x) => BankAccount.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "totalPages": totalPages,
    "bankAccounts": List<dynamic>.from(bankAccounts.map((x) => x.toJson())),
  };
}