import 'dart:convert';

import 'package:barkeaty/shared/models/direction_model.dart';

GetDirectionsResponse getDirectionsResponseFromJson(String str) => GetDirectionsResponse.fromJson(json.decode(str));

String getDirectionsResponseToJson(GetDirectionsResponse data) => json.encode(data.toJson());

class GetDirectionsResponse {
  GetDirectionsResponse({
    required this.status,
    required this.totalPages,
    required this.directions,
  });

  int status;
  int totalPages;
  List<Direction> directions;

  factory GetDirectionsResponse.fromJson(Map<String, dynamic> json) => GetDirectionsResponse(
    status: json["status"],
    totalPages: json["totalPages"],
    directions: List<Direction>.from(json["directions"].map((x) => Direction.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "totalPages": totalPages,
    "directions": List<dynamic>.from(directions.map((x) => x.toJson())),
  };
}