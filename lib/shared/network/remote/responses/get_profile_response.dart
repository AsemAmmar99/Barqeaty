import 'dart:convert';

import 'package:barkeaty/shared/models/account_model.dart';

GetProfileResponse getProfileResponseFromJson(String str) => GetProfileResponse.fromJson(json.decode(str));

String getProfileResponseToJson(GetProfileResponse data) => json.encode(data.toJson());

class GetProfileResponse {
  GetProfileResponse({
    required this.status,
    required this.myAccount,
  });

  int status;
  Account myAccount;

  factory GetProfileResponse.fromJson(Map<String, dynamic> json) => GetProfileResponse(
    status: json["status"],
    myAccount: Account.fromJson(json["myAccount"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "myAccount": myAccount.toJson(),
  };
}