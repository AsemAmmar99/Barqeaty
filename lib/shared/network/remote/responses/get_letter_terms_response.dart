import 'dart:convert';

import 'package:barkeaty/shared/models/terms_data_model.dart';

GetLetterTermsResponse getLetterTermsResponseFromJson(String str) => GetLetterTermsResponse.fromJson(json.decode(str));

String getLetterTermsResponseToJson(GetLetterTermsResponse data) => json.encode(data.toJson());

class GetLetterTermsResponse {
  GetLetterTermsResponse({
    required this.status,
    required this.termsData,
  });

  int status;
  TermsData termsData;

  factory GetLetterTermsResponse.fromJson(Map<String, dynamic> json) => GetLetterTermsResponse(
    status: json["status"],
    termsData: TermsData.fromJson(json["termsData"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "termsData": termsData.toJson(),
  };
}