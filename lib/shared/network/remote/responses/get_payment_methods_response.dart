import 'dart:convert';

import 'package:barkeaty/shared/models/payment_method_model.dart';

GetPaymentMethodsResponse getPaymentMethodsResponseFromJson(String str) => GetPaymentMethodsResponse.fromJson(json.decode(str));

String getPaymentMethodsResponseToJson(GetPaymentMethodsResponse data) => json.encode(data.toJson());

class GetPaymentMethodsResponse {
  GetPaymentMethodsResponse({
    required this.status,
    required this.totalPages,
    required this.paymentMethods,
  });

  int status;
  int totalPages;
  List<PaymentMethod> paymentMethods;

  factory GetPaymentMethodsResponse.fromJson(Map<String, dynamic> json) => GetPaymentMethodsResponse(
    status: json["status"],
    totalPages: json["totalPages"],
    paymentMethods: List<PaymentMethod>.from(json["paymentMethods"].map((x) => PaymentMethod.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "totalPages": totalPages,
    "paymentMethods": List<dynamic>.from(paymentMethods.map((x) => x.toJson())),
  };
}