import 'dart:convert';

import 'package:barkeaty/shared/models/private_company_model.dart';

GetPrivateCompaniesResponse getPrivateCompaniesResponseFromJson(String str) => GetPrivateCompaniesResponse.fromJson(json.decode(str));

String getPrivateCompaniesResponseToJson(GetPrivateCompaniesResponse data) => json.encode(data.toJson());

class GetPrivateCompaniesResponse {
  GetPrivateCompaniesResponse({
    required this.status,
    required this.totalPages,
    required this.privateCompanies,
  });

  int status;
  int totalPages;
  List<PrivateCompany> privateCompanies;

  factory GetPrivateCompaniesResponse.fromJson(Map<String, dynamic> json) => GetPrivateCompaniesResponse(
    status: json["status"],
    totalPages: json["totalPages"],
    privateCompanies: List<PrivateCompany>.from(json["privateCompanies"].map((x) => PrivateCompany.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "totalPages": totalPages,
    "privateCompanies": List<dynamic>.from(privateCompanies.map((x) => x.toJson())),
  };
}