import 'dart:convert';

import 'package:barkeaty/shared/models/my_letter_model.dart';

GetMyLettersResponse getMyLettersResponseFromJson(String str) => GetMyLettersResponse.fromJson(json.decode(str));

String getMyLettersResponseToJson(GetMyLettersResponse data) => json.encode(data.toJson());

class GetMyLettersResponse {
  GetMyLettersResponse({
    required this.status,
    required this.totalPages,
    required this.letters,
  });

  int status;
  int totalPages;
  List<MyLetter> letters;

  factory GetMyLettersResponse.fromJson(Map<String, dynamic> json) => GetMyLettersResponse(
    status: json["status"],
    totalPages: json["totalPages"],
    letters: List<MyLetter>.from(json["letters"].map((x) => MyLetter.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "totalPages": totalPages,
    "letters": List<dynamic>.from(letters.map((x) => x.toJson())),
  };
}