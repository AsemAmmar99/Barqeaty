import 'dart:convert';

import 'package:barkeaty/shared/models/terms_data_model.dart';

GetLetterBeforeStartResponse getLetterBeforeStartResponseFromJson(String str) => GetLetterBeforeStartResponse.fromJson(json.decode(str));

String getLetterBeforeStartResponseToJson(GetLetterBeforeStartResponse data) => json.encode(data.toJson());

class GetLetterBeforeStartResponse {
  GetLetterBeforeStartResponse({
    required this.status,
    required this.termsData,
  });

  int status;
  TermsData termsData;

  factory GetLetterBeforeStartResponse.fromJson(Map<String, dynamic> json) => GetLetterBeforeStartResponse(
    status: json["status"],
    termsData: TermsData.fromJson(json["termsData"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "termsData": termsData.toJson(),
  };
}
