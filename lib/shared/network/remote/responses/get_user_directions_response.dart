import 'dart:convert';

import 'package:barkeaty/shared/models/gud_private_company_model.dart';

GetUserDirectionsResponse getUserDirectionsResponseFromJson(String str) => GetUserDirectionsResponse.fromJson(json.decode(str));

String getUserDirectionsResponseToJson(GetUserDirectionsResponse data) => json.encode(data.toJson());

class GetUserDirectionsResponse {
  GetUserDirectionsResponse({
    required this.status,
    required this.totalPages,
    required this.privateCompanies,
  });

  int status;
  int totalPages;
  List<GUDPrivateCompany> privateCompanies;

  factory GetUserDirectionsResponse.fromJson(Map<String, dynamic> json) => GetUserDirectionsResponse(
    status: json["status"],
    totalPages: json["totalPages"],
    privateCompanies: List<GUDPrivateCompany>.from(json["privateCompanies"].map((x) => GUDPrivateCompany.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "totalPages": totalPages,
    "privateCompanies": List<dynamic>.from(privateCompanies.map((x) => x.toJson())),
  };
}