import 'dart:convert';

import 'package:barkeaty/shared/models/account_model.dart';

UpdateProfileResponse updateProfileResponseFromJson(String str) => UpdateProfileResponse.fromJson(json.decode(str));

String updateProfileResponseToJson(UpdateProfileResponse data) => json.encode(data.toJson());

class UpdateProfileResponse {
  UpdateProfileResponse({
    required this.status,
    required this.account,
    required this.tmpToken,
  });

  int status;
  Account account;
  String tmpToken;

  factory UpdateProfileResponse.fromJson(Map<String, dynamic> json) => UpdateProfileResponse(
    status: json["status"],
    account: Account.fromJson(json["account"]),
    tmpToken: json["tmpToken"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "account": account.toJson(),
    "tmpToken": tmpToken,
  };
}