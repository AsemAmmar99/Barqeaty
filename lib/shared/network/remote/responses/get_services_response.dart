import 'dart:convert';

import 'package:barkeaty/shared/models/service_model.dart';

GetServicesResponse getServicesResponseFromJson(String str) => GetServicesResponse.fromJson(json.decode(str));

String getServicesResponseToJson(GetServicesResponse data) => json.encode(data.toJson());

class GetServicesResponse {
  GetServicesResponse({
    required this.status,
    required this.totalPages,
    required this.services,
  });

  int status;
  int totalPages;
  List<Service> services;

  factory GetServicesResponse.fromJson(Map<String, dynamic> json) => GetServicesResponse(
    status: json["status"],
    totalPages: json["totalPages"],
    services: List<Service>.from(json["services"].map((x) => Service.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "totalPages": totalPages,
    "services": List<dynamic>.from(services.map((x) => x.toJson())),
  };
}