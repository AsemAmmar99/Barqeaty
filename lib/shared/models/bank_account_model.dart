class BankAccount {
  BankAccount({
    required this.id,
    required this.name,
    required this.accountNumber,
    required this.ibanNumber,
    required this.logo,
  });

  int id;
  String name;
  String accountNumber;
  String ibanNumber;
  String logo;

  factory BankAccount.fromJson(Map<String, dynamic> json) => BankAccount(
    id: json["id"],
    name: json["name"],
    accountNumber: json["accountNumber"],
    ibanNumber: json["ibanNumber"],
    logo: json["logo"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "accountNumber": accountNumber,
    "ibanNumber": ibanNumber,
    "logo": logo,
  };
}
