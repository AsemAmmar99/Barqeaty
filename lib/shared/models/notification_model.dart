class Notification {
  Notification({
    required this.id,
    required this.content,
    required this.createdAt,
    required this.createdAtInt,
  });

  int id;
  String content;
  DateTime createdAt;
  int createdAtInt;

  factory Notification.fromJson(Map<String, dynamic> json) => Notification(
    id: json["id"] ?? 0,
    content: json["content"] ?? '',
    createdAt: DateTime.parse(json["created_at"] ?? 0),
    createdAtInt: json["created_at_int"] ?? 0,
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "content": content,
    "created_at": createdAt.toIso8601String(),
    "created_at_int": createdAtInt,
  };
}
