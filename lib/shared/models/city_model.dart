class City {
  City({
    required this.id,
    required this.name,
  });

  int id;
  String name;

  factory City.fromJson(Map<String, dynamic> json) => City(
    id: json["id"] ?? '1',
    name: json["name"] ?? 'اختر مدينتك..',
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
  };
}
