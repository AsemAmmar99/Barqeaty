class Coupon {
  Coupon({
    required this.id,
    required this.startDate,
    required this.endDate,
    required this.discount,
  });

  int id;
  DateTime startDate;
  DateTime endDate;
  int discount;

  factory Coupon.fromJson(Map<String, dynamic> json) => Coupon(
    id: json["id"] ?? 0,
    startDate: DateTime.parse(json["startDate"] ?? 0),
    endDate: DateTime.parse(json["endDate"] ?? 0),
    discount: json["discount"] ?? 0,
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "startDate": "${startDate.year.toString().padLeft(4, '0')}-${startDate.month.toString().padLeft(2, '0')}-${startDate.day.toString().padLeft(2, '0')}",
    "endDate": "${endDate.year.toString().padLeft(4, '0')}-${endDate.month.toString().padLeft(2, '0')}-${endDate.day.toString().padLeft(2, '0')}",
    "discount": discount,
  };
}
