class GUDPrivateCompany {
  GUDPrivateCompany({
    required this.id,
    required this.name,
    required this.maiCity,
    required this.tel,
    required this.mailRegion,
    required this.mailBuilding,
    required this.mailUnitNumber,
    required this.mailPostalCode,
    required this.mailAdditionalCode,
    required this.mobile,
  });

  int id;
  String name;
  String maiCity;
  String tel;
  String mailRegion;
  String mailBuilding;
  String mailUnitNumber;
  String mailPostalCode;
  String mailAdditionalCode;
  String mobile;

  factory GUDPrivateCompany.fromJson(Map<String, dynamic> json) => GUDPrivateCompany(
    id: json["id"],
    name: json["name"],
    maiCity: json["maiCity"] == null ? null : json["maiCity"],
    tel: json["tel"] == null ? null : json["tel"],
    mailRegion: json["mailRegion"] == null ? null : json["mailRegion"],
    mailBuilding: json["mailBuilding"] == null ? null : json["mailBuilding"],
    mailUnitNumber: json["mailUnitNumber"] == null ? null : json["mailUnitNumber"],
    mailPostalCode: json["mailPostalCode"] == null ? null : json["mailPostalCode"],
    mailAdditionalCode: json["mailAdditionalCode"] == null ? null : json["mailAdditionalCode"],
    mobile: json["mobile"] == null ? null : json["mobile"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "maiCity": maiCity == null ? null : maiCity,
    "tel": tel == null ? null : tel,
    "mailRegion": mailRegion == null ? null : mailRegion,
    "mailBuilding": mailBuilding == null ? null : mailBuilding,
    "mailUnitNumber": mailUnitNumber == null ? null : mailUnitNumber,
    "mailPostalCode": mailPostalCode == null ? null : mailPostalCode,
    "mailAdditionalCode": mailAdditionalCode == null ? null : mailAdditionalCode,
    "mobile": mobile == null ? null : mobile,
  };
}
