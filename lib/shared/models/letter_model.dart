import 'package:barkeaty/shared/models/direction_model.dart';
import 'package:barkeaty/shared/models/letter_type_model.dart';
import 'package:barkeaty/shared/models/private_company_model.dart';
import 'package:barkeaty/shared/models/service_model.dart';

import 'attachment_model.dart';

class Letter {
  Letter({
    required this.id,
    required this.createdAt,
    required this.createdAtInt,
    this.privateCompany,
    this.direction,
    required this.service,
    required this.totalCost,
    required this.letterText,
    required this.attachCount,
    required this.wordsCount,
    required this.extraWordsCount,
    required this.extraWordsPrice,
    required this.attachmentsCount,
    required this.extraAttachmentsCount,
    required this.extraAttachmentsPrice,
    required this.letterType,
    required this.attachments,
    required this.discount,
  });

  int id;
  DateTime createdAt;
  int createdAtInt;
  PrivateCompany? privateCompany;
  Direction? direction;
  Service service;
  int totalCost;
  String letterText;
  int attachCount;
  int wordsCount;
  int extraWordsCount;
  int extraWordsPrice;
  int attachmentsCount;
  int extraAttachmentsCount;
  int extraAttachmentsPrice;
  int discount;
  LetterType letterType;
  List<Attachment> attachments;

  factory Letter.fromJson(Map<String, dynamic> json) => Letter(
        id: json["id"],
        createdAt: DateTime.parse(json["created_at"]),
        createdAtInt: json["created_at_int"],
        privateCompany: json["privateCompany"] == null
            ? null
            : PrivateCompany.fromJson(json["privateCompany"]),
        direction: json["direction"] == null
            ? null
            : Direction.fromJson(json["direction"]),
        service: Service.fromJson(json["service"]),
        totalCost: json["totalCost"],
        letterText: json["letterText"],
        attachCount: json["attachCount"],
        wordsCount: json["wordsCount"],
        extraWordsCount: json["extraWordsCount"],
        extraWordsPrice: json["extraWordsPrice"],
        attachmentsCount: json["attachmentsCount"],
        extraAttachmentsCount: json["extraAttachmentsCount"],
        extraAttachmentsPrice: json["extraAttachmentsPrice"],
        letterType: LetterType.fromJson(json["letterType"]),
        attachments: List<Attachment>.from(
            json["attachments"].map((x) => Attachment.fromJson(x))),
        discount: json['discount'] ?? 0,
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "created_at": createdAt.toIso8601String(),
        "created_at_int": createdAtInt,
        "privateCompany": privateCompany!.toJson(),
        "direction": direction!.toJson(),
        "service": service.toJson(),
        "totalCost": totalCost,
        "letterText": letterText,
        "attachCount": attachCount,
        "wordsCount": wordsCount,
        "extraWordsCount": extraWordsCount,
        "extraWordsPrice": extraWordsPrice,
        "attachmentsCount": attachmentsCount,
        "extraAttachmentsCount": extraAttachmentsCount,
        "extraAttachmentsPrice": extraAttachmentsPrice,
        "letterType": letterType.toJson(),
        "discount": discount,
        "attachments": List<dynamic>.from(attachments.map((x) => x.toJson())),
      };
}
