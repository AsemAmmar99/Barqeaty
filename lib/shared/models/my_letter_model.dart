import 'package:barkeaty/shared/models/direction_model.dart';
import 'package:barkeaty/shared/models/private_company_model.dart';
import 'package:barkeaty/shared/models/status_model.dart';

class MyLetter {
  MyLetter({
    required this.id,
    required this.createdAt,
    required this.createdAtInt,
    this.privateCompany,
    required this.status,
    this.direction,
  });

  int id;
  DateTime createdAt;
  int createdAtInt;
  PrivateCompany? privateCompany;
  Status status;
  Direction? direction;

  factory MyLetter.fromJson(Map<String, dynamic> json) => MyLetter(
    id: json["id"],
    createdAt: DateTime.parse(json["created_at"]),
    createdAtInt: json["created_at_int"],
    privateCompany: json["privateCompany"] == null ? null : PrivateCompany.fromJson(json["privateCompany"]),
    status: Status.fromJson(json["status"]),
    direction: json["direction"] == null ? null : Direction.fromJson(json["direction"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "created_at": createdAt.toIso8601String(),
    "created_at_int": createdAtInt,
    "privateCompany": privateCompany == null ? null : privateCompany!.toJson(),
    "status": status.toJson(),
    "direction": direction == null ? null : direction!.toJson(),
  };
}