class PaymentMethod {
  PaymentMethod({
    required this.id,
    required this.name,
    required this.paytab,
    required this.icon,
  });

  int id;
  String name;
  String paytab;
  String icon;

  factory PaymentMethod.fromJson(Map<String, dynamic> json) => PaymentMethod(
    id: json["id"],
    name: json["name"],
    paytab: json["paytab"],
    icon: json["icon"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "paytab": paytab,
    "icon": icon,
  };
}
