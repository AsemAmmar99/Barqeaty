class Template {
  Template({
    required this.id,
    required this.name,
    required this.text,
  });

  int id;
  String name;
  String text;

  factory Template.fromJson(Map<String, dynamic> json) => Template(
    id: json["id"],
    name: json["name"],
    text: json["text"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "text": text,
  };
}
