import 'city_model.dart';

class Region {
  Region({
    required this.id,
    required this.name,
    required this.cities,
  });

  int id;
  String name;
  List<City> cities;

  factory Region.fromJson(Map<String, dynamic> json) => Region(
    id: json["id"],
    name: json["name"],
    cities: List<City>.from(json["cities"].map((x) => City.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "cities": List<dynamic>.from(cities.map((x) => x.toJson())),
  };
}