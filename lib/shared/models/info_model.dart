class Info {
  Info({
    required this.letterStartAlertText,
    required this.letterStartAlertEnable,
    required this.mainPageText,
    required this.letterBeforeStartText,
    required this.textBelowLetterBox,
    required this.prePaymentAlertText,
    required this.prePaymentAlertEnable,
    required this.successPaymentText,
    required this.failedPaymentText,
  });

  String letterStartAlertText;
  bool letterStartAlertEnable;
  String mainPageText;
  String letterBeforeStartText;
  String textBelowLetterBox;
  String prePaymentAlertText;
  bool prePaymentAlertEnable;
  String successPaymentText;
  String failedPaymentText;

  factory Info.fromJson(Map<String, dynamic> json) => Info(
    letterStartAlertText: json["letterStartAlertText"],
    letterStartAlertEnable: json["letterStartAlertEnable"],
    mainPageText: json["mainPageText"],
    letterBeforeStartText: json["letterBeforeStartText"],
    textBelowLetterBox: json["textBelowLetterBox"],
    prePaymentAlertText: json["prePaymentAlertText"],
    prePaymentAlertEnable: json["prePaymentAlertEnable"],
    successPaymentText: json["successPaymentText"],
    failedPaymentText: json["failedPaymentText"],
  );

  Map<String, dynamic> toJson() => {
    "letterStartAlertText": letterStartAlertText,
    "letterStartAlertEnable": letterStartAlertEnable,
    "mainPageText": mainPageText,
    "letterBeforeStartText": letterBeforeStartText,
    "textBelowLetterBox": textBelowLetterBox,
    "prePaymentAlertText": prePaymentAlertText,
    "prePaymentAlertEnable": prePaymentAlertEnable,
    "successPaymentText": successPaymentText,
    "failedPaymentText": failedPaymentText,
  };
}
