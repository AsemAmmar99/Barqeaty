class PrivateCompany {
  PrivateCompany({
    required this.id,
    required this.name,
    required this.tradingName,
    required this.tadawolName,
    required this.intro,
    required this.contactPerson,
    required this.tel,
    required this.fax,
  });

  int id;
  String name;
  String tradingName;
  String tadawolName;
  String intro;
  String contactPerson;
  String tel;
  String fax;

  factory PrivateCompany.fromJson(Map<String, dynamic> json) => PrivateCompany(
    id: json["id"],
    name: json["name"],
    tradingName: json["tradingName"],
    tadawolName: json["tadawolName"],
    intro: json["intro"],
    contactPerson: json["contactPerson"],
    tel: json["tel"],
    fax: json["fax"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "tradingName": tradingName,
    "tadawolName": tadawolName,
    "intro": intro,
    "contactPerson": contactPerson,
    "tel": tel,
    "fax": fax,
  };
}
