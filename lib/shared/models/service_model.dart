class Service {
  Service({
    required this.id,
    required this.name,
    required this.wordPrice,
    required this.minCost,
    required this.minWordsCount,
    required this.rowColor,
    required this.icon,
    required this.desc1,
    required this.desc2,
    required this.attachPrice,
    required this.freeAttach,
    required this.desc,
    required this.vat,
  });

  int id;
  String name;
  double wordPrice;
  double minCost;
  int minWordsCount;
  String rowColor;
  String icon;
  String desc1;
  String desc2;
  int attachPrice;
  int freeAttach;
  int vat;
  String desc;

  factory Service.fromJson(Map<String, dynamic> json) => Service(
    id: json["id"],
    name: json["name"],
    wordPrice: json["wordPrice"].toDouble(),
    minCost: json["minCost"].toDouble(),
    minWordsCount: json["minWordsCount"],
    rowColor: json["rowColor"],
    icon: json["icon"],
    desc1: json["desc1"],
    desc2: json["desc2"],
    attachPrice: json["attachPrice"],
    freeAttach: json["freeAttach"],
    desc: json["desc"],
    vat: json["vat"] ?? 0,
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "wordPrice": wordPrice,
    "minCost": minCost,
    "minWordsCount": minWordsCount,
    "rowColor": rowColor,
    "icon": icon,
    "desc1": desc1,
    "desc2": desc2,
    "attachPrice": attachPrice,
    "freeAttach": freeAttach,
    "desc": desc,
    "vat" : vat,
  };
}