class LetterType {
  LetterType({
    required this.id,
    required this.name,
    required this.minPrice,
    required this.maxPrice,
  });

  int id;
  String name;
  int minPrice;
  int maxPrice;

  factory LetterType.fromJson(Map<String, dynamic> json) => LetterType(
    id: json["id"],
    name: json["name"],
    minPrice: json["minPrice"],
    maxPrice: json["maxPrice"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "minPrice": minPrice,
    "maxPrice": maxPrice,
  };
}
