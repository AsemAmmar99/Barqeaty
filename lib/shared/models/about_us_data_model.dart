class AboutUsData {
  AboutUsData({
    required this.notesForFlutter,
    required this.text,
  });

  String notesForFlutter;
  String text;

  factory AboutUsData.fromJson(Map<String, dynamic> json) => AboutUsData(
    notesForFlutter: json["notesForFlutter"],
    text: json["text"],
  );

  Map<String, dynamic> toJson() => {
    "notesForFlutter": notesForFlutter,
    "text": text,
  };
}