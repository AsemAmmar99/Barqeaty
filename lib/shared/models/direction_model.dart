class Direction {
  Direction({
    required this.id,
    required this.name,
  });

  int id;
  String name;

  factory Direction.fromJson(Map<String, dynamic> json) => Direction(
    id: json["id"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
  };
}
