class Attachment {
  Attachment({
    required this.id,
    required this.fileType,
    required this.fileName,
    required this.filePath,
    required this.fileSize,
  });

  int id;
  String fileType;
  String fileName;
  String filePath;
  String fileSize;

  factory Attachment.fromJson(Map<String, dynamic> json) => Attachment(
    id: json["id"],
    fileType: json["fileType"],
    fileName: json["fileName"],
    filePath: json["filePath"],
    fileSize: json["fileSize"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "fileType": fileType,
    "fileName": fileName,
    "filePath": filePath,
    "fileSize": fileSize,
  };
}