import 'district_model.dart';

class Account {
  Account({
    required this.id,
    required this.apiToken,
    required this.name,
    required this.lang,
    required this.phone,
    required this.idNumber,
    required this.district,
    required this.email,
  });

  int id;
  String apiToken;
  String name;
  String lang;
  String phone;
  String idNumber;
  District district;
  String email;

  factory Account.fromJson(Map<String, dynamic> json) => Account(
    id: json["id"],
    apiToken: json["apiToken"],
    name: json["name"],
    lang: json["lang"],
    phone: json["phone"],
    idNumber: json["idNumber"],
    district: District.fromJson(json["district"]),
    email: json["email"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "apiToken": apiToken,
    "name": name,
    "lang": lang,
    "phone": phone,
    "idNumber": idNumber,
    "district": district.toJson(),
    "email": email,
  };
}