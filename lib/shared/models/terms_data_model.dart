class TermsData {
  TermsData({
    required this.notesForFlutter,
    required this.text,
  });

  String notesForFlutter;
  String text;

  factory TermsData.fromJson(Map<String, dynamic> json) => TermsData(
    notesForFlutter: json["notesForFlutter"],
    text: json["text"],
  );

  Map<String, dynamic> toJson() => {
    "notesForFlutter": notesForFlutter,
    "text": text,
  };
}